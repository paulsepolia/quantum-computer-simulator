(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 9.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1063,         20]
NotebookDataLength[    121416,       4322]
NotebookOptionsPosition[    100666,       3741]
NotebookOutlinePosition[    103835,       3817]
CellTagsIndexPosition[    103792,       3814]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"Quit", "[", "]"}]], "Input"],

Cell[CellGroupData[{

Cell["The QCS Dense", "Title"],

Cell[CellGroupData[{

Cell["1 \[LongRightArrow] Introduction", "Section"],

Cell[CellGroupData[{

Cell["1 \[LongRightArrow] The Purpose", "Subsection"],

Cell[TextData[{
 "In this research text I present all the fundamentals quantum computer \
operators and I make use of the corresponding quantum computer algebra to \
develop a quantum computer simulator. The main functions are ",
 StyleBox["1. ",
  FontWeight->"Bold"],
 "the ",
 StyleBox["stateVectorFunction1[list___]\[Congruent]SVF1[list___],",
  FontWeight->"Bold"],
 " which takes as arguments an arbitrary sequence of total ",
 StyleBox["n",
  FontWeight->"Bold"],
 " up and/or down qubits and constructs the initial n-qubit,",
 StyleBox[" 2. ",
  FontWeight->"Bold"],
 "the ",
 StyleBox["GeneralArrayFlattenOuterNoRam[stepFun_, OpT_] ",
  FontWeight->"Bold"],
 "function which takes as argument the initial n-qubit and the operators that \
act up it and evaluates the rest n-qubits. ",
 StyleBox["3. ",
  FontWeight->"Bold"],
 "the",
 StyleBox[" plotQCS[svFun_, {stepIn_, stepFin_}, {pos1_, pos2_}, opts___] ",
  FontWeight->"Bold"],
 "which",
 StyleBox[" ",
  FontWeight->"Bold"],
 "visualizes the states of the quantum computer."
}], "Text"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "2 \[LongRightArrow] Chapter 1 \[LongRightArrow]",
 StyleBox[" ",
  FontSlant->"Italic"],
 "The Quantum State Vectors"
}], "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 "1 \[LongRightArrow] The",
 StyleBox[" ",
  FontSlant->"Italic"],
 "qubit[{theta_,phi_}] "
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["qubit[{theta_,phi_}] ",
  FontWeight->"Bold"],
 "function, which gives ",
 StyleBox["the most general formalism ",
  FontWeight->"Bold"],
 "of ",
 StyleBox["a",
  FontWeight->"Bold"],
 " ",
 StyleBox["single",
  FontWeight->"Bold"],
 " ",
 StyleBox["qubit",
  FontWeight->"Bold"],
 " as a function of the variables ",
 StyleBox["theta ",
  FontWeight->"Bold"],
 "and",
 StyleBox[" phi.",
  FontWeight->"Bold"],
 "  "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qubit", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", 
   RowBox[{"theta", ",", "phi"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"theta", ",", "phi"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"theta", ",", "phi"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"qubit", "[", 
    RowBox[{"{", 
     RowBox[{"theta_", ",", "phi_"}], "}"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"Cos", "[", 
       FractionBox["theta", "2"], "]"}], ",", "0"}], "}"}], "+", 
    RowBox[{
     RowBox[{"Exp", "[", 
      RowBox[{"I", "*", "phi"}], "]"}], "*", 
     RowBox[{"{", 
      RowBox[{"0", ",", 
       RowBox[{"Sin", "[", 
        FractionBox["theta", "2"], "]"}]}], "}"}]}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qubit", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qubit", "[", 
   RowBox[{"{", 
    RowBox[{"theta", ",", "phi"}], "}"}], "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general arbitrary qubit.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{
   RowBox[{"qubit", "[", 
    RowBox[{"{", 
     RowBox[{"theta", ",", "phi"}], "}"}], "]"}], "=", 
   TagBox[
    RowBox[{"(", "\[NoBreak]", 
     TagBox[GridBox[{
        {
         RowBox[{"Cos", "[", 
          FractionBox["theta", "2"], "]"}]},
        {
         RowBox[{
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{"\[ImaginaryI]", " ", "phi"}]], " ", 
          RowBox[{"Sin", "[", 
           FractionBox["theta", "2"], "]"}]}]}
       },
       GridBoxAlignment->{
        "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
          "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.5599999999999999]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}],
      Column], "\[NoBreak]", ")"}],
    Function[BoxForm`e$, 
     MatrixForm[BoxForm`e$]]]}],
  FontFamily->"Times New Roman"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] The generalStateVector[{theta_,phi_},dimension_]\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["generalStateVector[{theta_,phi_},dimension_] ",
  FontWeight->"Bold"],
 "function, which gives ",
 StyleBox["the most general formalism",
  FontWeight->"Bold"],
 " of ",
 StyleBox["a",
  FontWeight->"Bold"],
 " ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["dimension",
  FontWeight->"Bold"],
 " ",
 StyleBox["qubit",
  FontWeight->"Bold"],
 " as a function of the variables ",
 StyleBox["theta[i] ",
  FontWeight->"Bold"],
 "and",
 StyleBox[" phi[i].",
  FontWeight->"Bold"],
 "  "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "generalStateVector", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"generalStateVector", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"theta_", ",", "phi_"}], "}"}], ",", "dimension_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Flatten", "@", 
    RowBox[{"Outer", "[", 
     RowBox[{"Times", ",", 
      RowBox[{"Apply", "[", 
       RowBox[{"Sequence", ",", 
        RowBox[{"Table", "[", 
         RowBox[{
          RowBox[{"qubit", "[", 
           RowBox[{"{", 
            RowBox[{
             RowBox[{"theta", "[", "i", "]"}], ",", 
             RowBox[{"phi", "[", "i", "]"}]}], "}"}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"i", ",", "1", ",", "dimension"}], "}"}]}], "]"}]}], 
       "]"}]}], "]"}]}], "/;", 
   RowBox[{
    RowBox[{"dimension", "\[GreaterEqual]", "1"}], "&&", 
    RowBox[{"dimension", "\[Element]", "Integers"}]}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "generalStateVector", "]"}], ";"}]], "Input"],

Cell[TextData[{
 "Example 1. Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general arbitrary",
  FontWeight->"Bold"],
 " ",
 StyleBox["1",
  FontWeight->"Bold"],
 "-",
 StyleBox["dimension qubit.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"generalStateVector", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"theta", ",", "phi"}], "}"}], ",", "1"}], "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Example 2. Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general arbitrary",
  FontWeight->"Bold"],
 " ",
 StyleBox["2",
  FontWeight->"Bold"],
 "-",
 StyleBox["dimensions qubit.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"generalStateVector", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"theta", ",", "phi"}], "}"}], ",", "2"}], "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Example 3. Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general arbitrary",
  FontWeight->"Bold"],
 " ",
 StyleBox["3",
  FontWeight->"Bold"],
 "-",
 StyleBox["dimensions qubit.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"generalStateVector", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"theta", ",", "phi"}], "}"}], ",", "3"}], "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
3 \[LongRightArrow] Chapter 2 \[LongRightArrow] The Quantum Operators\
\>", "Section"],

Cell[CellGroupData[{

Cell["\<\
1 \[LongRightArrow] The generalQuantumOperator22[a,b,c,d] \[Congruent] \
gQO22[a,b,c,d] \
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["generalQuantumOperator22[a_,b_,c_,d_] \[Congruent]",
  FontWeight->"Bold"],
 " ",
 StyleBox["gQO22[a_,b_,c_,d_]",
  FontWeight->"Bold"],
 " function, which gives the ",
 StyleBox["most general formalism",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["2\[Times]2 Quantum Hermitian Operator ",
  FontWeight->"Bold"],
 "as a function of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Reals {a,b,c,d}.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "generalQuantumOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "gQO22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", 
   RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"generalQuantumOperator22", "[", 
    RowBox[{"a_", ",", "b_", ",", "c_", ",", "d_"}], "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", "opTmp", "}"}], ",", 
     RowBox[{
      RowBox[{
       RowBox[{"opTmp", "[", "1", "]"}], "=", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Exp", "[", 
            RowBox[{
             RowBox[{"-", "I"}], "*", 
             FractionBox["d", "2"]}], "]"}], ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", 
           RowBox[{"Exp", "[", 
            RowBox[{"I", "*", 
             FractionBox["d", "2"]}], "]"}]}], "}"}]}], "}"}]}], ";", 
      RowBox[{
       RowBox[{"opTmp", "[", "2", "]"}], "=", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Cos", "[", 
            FractionBox["c", "2"], "]"}], ",", 
           RowBox[{"-", 
            RowBox[{"Sin", "[", 
             FractionBox["c", "2"], "]"}]}]}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Sin", "[", 
            FractionBox["c", "2"], "]"}], ",", 
           RowBox[{"Cos", "[", 
            FractionBox["c", "2"], "]"}]}], "}"}]}], "}"}]}], ";", 
      RowBox[{
       RowBox[{"opTmp", "[", "3", "]"}], "=", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Exp", "[", 
            RowBox[{
             RowBox[{"-", "I"}], "*", 
             FractionBox["b", "2"]}], "]"}], ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", 
           RowBox[{"Exp", "[", 
            RowBox[{"I", "*", 
             FractionBox["b", "2"]}], "]"}]}], "}"}]}], "}"}]}], ";", 
      RowBox[{
       RowBox[{"Exp", "[", 
        RowBox[{"I", "*", "a"}], "]"}], "*", 
       RowBox[{"Dot", "[", 
        RowBox[{"Apply", "[", 
         RowBox[{"Sequence", ",", 
          RowBox[{"Table", "[", 
           RowBox[{
            RowBox[{"opTmp", "[", "i", "]"}], ",", 
            RowBox[{"{", 
             RowBox[{"i", ",", "3", ",", "1", ",", 
              RowBox[{"-", "1"}]}], "}"}]}], "]"}]}], "]"}], "]"}]}]}]}], 
    "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"gQO22", "[", 
    RowBox[{"a_", ",", "b_", ",", "c_", ",", "d_"}], "]"}], ":=", 
   RowBox[{"generalQuantumOperator22", "[", 
    RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "generalQuantumOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "gQO22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"FullSimplify", "[", 
   RowBox[{
    RowBox[{"gQO22", "[", 
     RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ",", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "}"}], "\[Element]", 
     "Reals"}]}], "]"}], "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general",
  FontWeight->"Bold"],
 " ",
 StyleBox["2\[Times]2 Quantum Hermitian Operator.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"gQO22", "[", 
    RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "=", 
   RowBox[{
    RowBox[{"generalQuantumOperator22", "[", 
     RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "=", 
    RowBox[{"(", "\[NoBreak]", GridBox[{
       {
        RowBox[{
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{
           FractionBox["1", "2"], " ", "\[ImaginaryI]", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"2", " ", "a"}], "-", "b", "-", "d"}], ")"}]}]], " ", 
         RowBox[{"Cos", "[", 
          FractionBox["c", "2"], "]"}]}], 
        RowBox[{
         RowBox[{"-", 
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{
            FractionBox["1", "2"], " ", "\[ImaginaryI]", " ", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{"2", " ", "a"}], "-", "b", "+", "d"}], ")"}]}]]}], " ", 
         RowBox[{"Sin", "[", 
          FractionBox["c", "2"], "]"}]}]},
       {
        RowBox[{
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{
           FractionBox["1", "2"], " ", "\[ImaginaryI]", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"2", " ", "a"}], "+", "b", "-", "d"}], ")"}]}]], " ", 
         RowBox[{"Sin", "[", 
          FractionBox["c", "2"], "]"}]}], 
        RowBox[{
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{
           FractionBox["1", "2"], " ", "\[ImaginaryI]", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"2", " ", "a"}], "+", "b", "+", "d"}], ")"}]}]], " ", 
         RowBox[{"Cos", "[", 
          FractionBox["c", "2"], "]"}]}]}
      },
      GridBoxAlignment->{
       "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
        "RowsIndexed" -> {}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.7]}, 
          Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}]}]}], ",", 
  " ", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "}"}], "\[Element]", 
   "Reals"}]}]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] The doNothingOperator22 \[Congruent] qI\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["doNothingOperator22 \[Congruent] qI.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "doNothingOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qI", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qI", "=", 
   RowBox[{"doNothingOperator22", "=", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"gQO22", "[", 
       RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "/.", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"a", "\[Rule]", "0"}], ",", 
        RowBox[{"b", "\[Rule]", "0"}], ",", 
        RowBox[{"c", "\[Rule]", "0"}], ",", 
        RowBox[{"d", "\[Rule]", "0"}]}], "}"}]}], ")"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "doNothingOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qI", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"qI", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"doNothingOperator22", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["2\[Times]2 doNothingOperator22.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{"qI", "=", 
  RowBox[{"doNothingOperator22", "=", 
   RowBox[{"(", "\[NoBreak]", GridBox[{
      {"1", "0"},
      {"0", "1"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.7]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", 
    ")"}]}]}]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "3 \[LongRightArrow] The phaseShiftOperator22[phi_] \[Congruent] qPh",
 StyleBox["[phi_]",
  FontWeight->"Bold"]
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["phaseShiftOperator22[phi_] \[Congruent] qPh[phi_].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "phaseShiftOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qPh", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"qPh", "[", "phi_", "]"}], ":=", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"gQO22", "[", 
       RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "/.", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"a", "\[Rule]", 
         FractionBox[
          RowBox[{"b", "+", "d"}], "2"]}], ",", 
        RowBox[{"c", "\[Rule]", "0"}]}], "}"}]}], "/.", 
     RowBox[{"{", 
      RowBox[{"b", "\[Rule]", 
       RowBox[{"phi", "-", "d"}]}], "}"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"phaseShiftOperator22", "[", "phi_", "]"}], ":=", 
   RowBox[{"qPh", "[", "phi", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "phaseShiftOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qPh", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qPh", "[", "phi", "]"}], "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["2\[Times]2 phaseShiftOperator22[phi].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qPh", "[", "phi", "]"}], "=", 
  RowBox[{
   RowBox[{"phaseShiftOperator22", "[", "phi", "]"}], "=", 
   RowBox[{"(", "\[NoBreak]", GridBox[{
      {"1", "0"},
      {"0", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"\[ImaginaryI]", " ", "phi"}]]}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.7]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", 
    ")"}]}]}]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "4 \[LongRightArrow] The hadamardOperator22 \[Congruent] qH",
 StyleBox[" ",
  FontWeight->"Bold",
  FontSlant->"Italic"]
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["hadamardOperator22 \[Congruent] qH.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qH", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "hadamardOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qH", "=", 
   RowBox[{"hadamardOperator22", "=", 
    RowBox[{
     RowBox[{"gQO22", "[", 
      RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "/.", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"a", "\[Rule]", 
        FractionBox["Pi", "2"]}], ",", 
       RowBox[{"b", "\[Rule]", 
        RowBox[{"3", "*", "Pi"}]}], ",", 
       RowBox[{"c", "\[Rule]", 
        FractionBox[
         RowBox[{"3", "*", "Pi"}], "2"]}], ",", 
       RowBox[{"d", "\[Rule]", "0"}]}], "}"}]}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qH", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "hadamardOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"qH", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"hadamardOperator22", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["hadamardOperator22.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{"qH", "=", 
   RowBox[{"hadamardOperator22", "=", 
    RowBox[{"(", "\[NoBreak]", GridBox[{
       {
        FractionBox["1", 
         SqrtBox["2"]], 
        FractionBox["1", 
         SqrtBox["2"]]},
       {
        FractionBox["1", 
         SqrtBox["2"]], 
        RowBox[{"-", 
         FractionBox["1", 
          SqrtBox["2"]]}]}
      },
      GridBoxAlignment->{
       "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
        "RowsIndexed" -> {}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.7]}, 
          Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}]}]}],
  FontFamily->"Times New Roman"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "5 \[LongRightArrow] The controlledNotOperator44 \[Congruent] ",
 StyleBox["qCN",
  FontWeight->"Bold"]
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["controlledNotOperator44 \[Congruent] qCN.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qCN", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "controlledNotOperator44", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qCN", "=", 
   RowBox[{"controlledNotOperator44", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"1", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0", ",", "0", ",", "0", ",", "1"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0", ",", "0", ",", "1", ",", "0"}], "}"}]}], "}"}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qCN", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "controlledNotOperator44", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"qCN", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"controlledNotOperator44", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["controlledNotOperator44.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{"qCN", "=", 
  RowBox[{"controlledNotOperator44", "=", 
   TagBox[
    StyleBox[
     RowBox[{"(", "\[NoBreak]", GridBox[{
        {"1", "0", "0", "0"},
        {"0", "1", "0", "0"},
        {"0", "0", "0", "1"},
        {"0", "0", "1", "0"}
       },
       GridBoxAlignment->{
        "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
          "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.7]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
     FontFamily->"Georgia"],
    Function[BoxForm`e$, 
     MatrixForm[BoxForm`e$]]]}]}]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
6 \[LongRightArrow] The controlledPhaseShiftOperator44[phi_] \[Congruent] \
qCPh[phi_]\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["controlledPhaseShiftOperator44[phi_] \[Congruent] qCPh[phi_].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qCPh", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "controlledPhaseShiftOperator44", "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"qCPh", "[", "phi_", "]"}], ":=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "1", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", 
       RowBox[{"Exp", "[", 
        RowBox[{"I", "*", "phi"}], "]"}]}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"controlledPhaseShiftOperator44", "[", "phi_", "]"}], ":=", 
   RowBox[{"qCPh", "[", "phi", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qCPh", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "controlledPhaseShiftOperator44", "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qCPh", "[", "phi", "]"}], "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"controlledPhaseShiftOperator44", "[", "phi", "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["controlledPhaseShiftOperator44[phi].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{
   RowBox[{"qCPh", "[", "phi", "]"}], "=", 
   RowBox[{
    RowBox[{"controlledPhaseShiftOperator44", "[", "phi", "]"}], "=", 
    TagBox[
     StyleBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
         {"1", "0", "0", "0"},
         {"0", "1", "0", "0"},
         {"0", "0", "1", "0"},
         {"0", "0", "0", 
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{"\[ImaginaryI]", " ", "phi"}]]}
        },
        GridBoxAlignment->{
         "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.7]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
      FontFamily->"Georgia"],
     Function[BoxForm`e$, 
      MatrixForm[BoxForm`e$]]]}]}],
  FontFamily->"Times New Roman"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
7 \[LongRightArrow] The controlledControlledNotOperator88 \[Congruent] q2CN\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["controlledControlledNotOperator88 \[Congruent] q2CN.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "q2CN", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "controlledControlledNotOperator88", "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"q2CN", "=", 
   RowBox[{"controlledControlledNotOperator88", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
       "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", 
        "1"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", 
        "0"}], "}"}]}], "}"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "q2CN", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "controlledControlledNotOperator88", "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"q2CN", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"controlledControlledNotOperator88", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["controlledControlledNotOperator88.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{"q2CN", "=", 
   RowBox[{"controlledControlledNotOperator88", "=", 
    TagBox[
     RowBox[{"(", "\[NoBreak]", GridBox[{
        {"1", "0", "0", "0", "0", "0", "0", "0"},
        {"0", "1", "0", "0", "0", "0", "0", "0"},
        {"0", "0", "1", "0", "0", "0", "0", "0"},
        {"0", "0", "0", "1", "0", "0", "0", "0"},
        {"0", "0", "0", "0", "1", "0", "0", "0"},
        {"0", "0", "0", "0", "0", "1", "0", "0"},
        {"0", "0", "0", "0", "0", "0", "0", "1"},
        {"0", "0", "0", "0", "0", "0", "1", "0"}
       },
       GridBoxAlignment->{
        "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
          "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.7]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
     Function[BoxForm`e$, 
      MatrixForm[BoxForm`e$]]]}]}],
  FontFamily->"Times New Roman"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"]
}, Open  ]],

Cell[CellGroupData[{

Cell["8 \[LongRightArrow] The fredkinOperator88 \[Congruent] qF", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["fredkinOperator88 \[Congruent] qF.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qF", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "fredkinOperator88", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qF", "=", 
   RowBox[{"fredkinOperator88", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
       "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", 
        "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", 
        "1"}], "}"}]}], "}"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qF", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "fredkinOperator88", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"qF", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"fredkinOperator88", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["fredkinOperator88.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{"qF", "=", 
   RowBox[{"fredkinOperator88", "=", 
    TagBox[
     RowBox[{"(", "\[NoBreak]", GridBox[{
        {"1", "0", "0", "0", "0", "0", "0", "0"},
        {"0", "1", "0", "0", "0", "0", "0", "0"},
        {"0", "0", "1", "0", "0", "0", "0", "0"},
        {"0", "0", "0", "1", "0", "0", "0", "0"},
        {"0", "0", "0", "0", "1", "0", "0", "0"},
        {"0", "0", "0", "0", "0", "0", "1", "0"},
        {"0", "0", "0", "0", "0", "1", "0", "0"},
        {"0", "0", "0", "0", "0", "0", "0", "1"}
       },
       GridBoxAlignment->{
        "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
          "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.7]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
     Function[BoxForm`e$, 
      MatrixForm[BoxForm`e$]]]}]}],
  FontFamily->"Georgia"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "4 \[LongRightArrow] Chapter 3 \[LongRightArrow] ",
 Cell[BoxData[
  FormBox[
   RowBox[{"The", " ", "Arbitrary", " ", "State", " ", "Vectors"}], 
   TraditionalForm]]]
}], "Section"],

Cell[CellGroupData[{

Cell["\<\
1 \[LongRightArrow] The up && down State Vectors of 1\[Dash]qubit\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["up",
  FontWeight->"Bold"],
 " and ",
 StyleBox["down",
  FontWeight->"Bold"],
 " qubits."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"up", ",", "down"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"up", "=", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"down", "=", 
   RowBox[{"{", 
    RowBox[{"0", ",", "1"}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"up", ",", "down"}], "]"}], ";"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] The Base State Vectors of n\[Dash]Qubits\
\>", "Subsection"],

Cell[CellGroupData[{

Cell[TextData[{
 "1 \[LongRightArrow] ",
 StyleBox["Definition of",
  FontWeight->"Plain"],
 " stateVectorFunction1[list___] ",
 StyleBox["function.",
  FontWeight->"Plain"]
}], "Subsubsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["stateVectorFunction1[list___] ",
  FontWeight->"Bold"],
 "function which takes as arquments a sequence of ",
 StyleBox["up ",
  FontWeight->"Bold"],
 "and/or ",
 StyleBox["down qubits ",
  FontWeight->"Bold"],
 "and gives",
 StyleBox[" ",
  FontWeight->"Bold"],
 "as result the \"matrix\" product of them. "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"stateVectorFunction1", ",", "SVF1"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stateVectorFunction1", "[", "list___", "]"}], ":=", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"Block", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"aTmp1", ",", "aTmp2", ",", "aTmp3", ",", "aTmp4"}], "}"}], 
       ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"aTmp1", "=", 
         RowBox[{"Length", "[", 
          RowBox[{"{", "list", "}"}], "]"}]}], ";", "\[IndentingNewLine]", 
        RowBox[{"Do", "[", 
         RowBox[{
          RowBox[{
           RowBox[{"If", "[", 
            RowBox[{
             RowBox[{"i", "\[Equal]", "1"}], ",", 
             RowBox[{"aTmp2", "=", 
              RowBox[{"Part", "[", 
               RowBox[{
                RowBox[{"{", "list", "}"}], ",", "1"}], "]"}]}], ",", 
             RowBox[{"aTmp2", "=", "aTmp4"}]}], "]"}], ";", 
           "\[IndentingNewLine]", 
           RowBox[{"aTmp3", "=", 
            RowBox[{"Part", "[", 
             RowBox[{
              RowBox[{"{", "list", "}"}], ",", 
              RowBox[{"i", "+", "1"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
           RowBox[{"aTmp4", "=", 
            RowBox[{"Flatten", "[", 
             RowBox[{"Outer", "[", 
              RowBox[{"Times", ",", "aTmp2", ",", "aTmp3"}], "]"}], "]"}]}]}],
           ",", "\[IndentingNewLine]", 
          RowBox[{"{", 
           RowBox[{"i", ",", "1", ",", 
            RowBox[{"aTmp1", "-", "1"}]}], "}"}]}], "]"}], ";", 
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{"aTmp1", "\[Equal]", "1"}], ",", 
          RowBox[{"aTmp4", "=", "list"}]}], "]"}], ";", "aTmp4"}]}], "]"}], 
     ")"}], "/;", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"MemberQ", "[", 
       RowBox[{
        RowBox[{"{", "list", "}"}], ",", "up"}], "]"}], "||", 
      RowBox[{"MemberQ", "[", 
       RowBox[{
        RowBox[{"{", "list", "}"}], ",", "down"}], "]"}]}], ")"}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"SVF1", "[", "list___", "]"}], ":=", 
   RowBox[{"stateVectorFunction1", "[", "list", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "stateVectorFunction1", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "SVF1", "]"}], ";"}]], "Input"],

Cell["Some Examples are the following.", "Text"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", "down", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{"up", ",", "up", ",", "down"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{"down", ",", "up", ",", "down", ",", "up"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{"down", ",", "up", ",", "up", ",", "down", ",", "down"}], 
  "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"SVF1", "[", 
  RowBox[{"down", ",", "up", ",", "up", ",", "down", ",", "down"}], 
  "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "2 \[LongRightArrow] ",
 StyleBox["Definition of",
  FontWeight->"Plain"],
 " upFunction[n_], downFunction[n] ",
 StyleBox["functions.",
  FontWeight->"Plain"],
 "  "
}], "Subsubsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["upFunction[n_] ",
  FontWeight->"Bold"],
 "and",
 StyleBox[" downFunction[n_] ",
  FontWeight->"Bold"],
 "functions which takes as arquments ",
 StyleBox["the integer valued variable",
  FontWeight->"Bold"],
 " ",
 StyleBox["n ",
  FontWeight->"Bold"],
 "and give back the \"matrix\" product of the ",
 StyleBox["n up ",
  FontWeight->"Bold"],
 "or",
 StyleBox[" down qubits.",
  FontWeight->"Bold"],
 " "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"upFunction", ",", "downFunction", ",", "upF", ",", "downF"}], 
   "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"upFunction", "[", "n_", "]"}], ":=", 
   RowBox[{"Apply", "[", 
    RowBox[{"Sequence", ",", 
     RowBox[{"Table", "[", 
      RowBox[{"up", ",", 
       RowBox[{"{", "n", "}"}]}], "]"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"downFunction", "[", "n_", "]"}], ":=", 
   RowBox[{"Apply", "[", 
    RowBox[{"Sequence", ",", 
     RowBox[{"Table", "[", 
      RowBox[{"down", ",", 
       RowBox[{"{", "n", "}"}]}], "]"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"upF", "[", "n_", "]"}], ":=", 
   RowBox[{"upFunction", "[", "n", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"downF", "[", "n_", "]"}], ":=", 
   RowBox[{"downFunction", "[", "n", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"upFunction", ",", "downFunction", ",", "upF", ",", "downF"}], 
   "]"}], ";"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["3 \[LongRightArrow] Some Examples", "Subsubsection"],

Cell[BoxData[
 RowBox[{"downFunction", "[", "4", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"downF", "[", "4", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"upFunction", "[", "3", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"upF", "[", "3", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"upF", "[", "10", "]"}], "===", 
  RowBox[{"upFunction", "[", "10", "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", "down", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{"up", ",", "up"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{"down", ",", "up", ",", 
   RowBox[{"downFunction", "[", "2", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{
   RowBox[{"downFunction", "[", "1", "]"}], ",", 
   RowBox[{"upFunction", "[", "2", "]"}], ",", 
   RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"SVF1", "[", 
  RowBox[{
   RowBox[{"downF", "[", "1", "]"}], ",", 
   RowBox[{"upF", "[", "2", "]"}], ",", 
   RowBox[{"downF", "[", "1", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"downFunction", "[", "11", "]"}], ",", 
    RowBox[{"upFunction", "[", "2", "]"}], ",", 
    RowBox[{"downFunction", "[", "1", "]"}]}], "]"}], "===", 
  RowBox[{"SVF1", "[", 
   RowBox[{
    RowBox[{"downF", "[", "11", "]"}], ",", 
    RowBox[{"upF", "[", "2", "]"}], ",", 
    RowBox[{"downF", "[", "1", "]"}]}], "]"}]}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
4 \[LongRightArrow] Definition of stateVectorFunction2[list___]\
\>", "Subsubsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["stateVectorFunction2[list___] ",
  FontWeight->"Bold"],
 "function which takes as arquments a sequence of ",
 StyleBox["up ",
  FontWeight->"Bold"],
 "and/or ",
 StyleBox["down qubits ",
  FontWeight->"Bold"],
 "and gives",
 StyleBox[" ",
  FontWeight->"Bold"],
 "as result the \"matrix\" product of them plus the ",
 StyleBox["dimension",
  FontWeight->"Bold"],
 " ",
 StyleBox["n ",
  FontWeight->"Bold"],
 "of the resulting ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubit,",
  FontWeight->"Bold"],
 " the ",
 StyleBox["array length ",
  FontWeight->"Bold"],
 "of the ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubit, ",
  FontWeight->"Bold"],
 "and finally the place location of the",
 StyleBox[" \"1\" ",
  FontWeight->"Bold"],
 "element in the array.",
 StyleBox[" ",
  FontWeight->"Bold"],
 "   "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"stateVectorFunction2", ",", "SVF2"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stateVectorFunction2", "[", "list___", "]"}], ":=", 
   RowBox[{
    RowBox[{"Block", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"aTmp1", ",", "aTmp2", ",", "aTmp3", ",", "aTmp4"}], "}"}], 
      ",", 
      RowBox[{
       RowBox[{"aTmp1", "=", 
        RowBox[{"{", 
         RowBox[{"Length", "[", 
          RowBox[{"{", 
           RowBox[{"Evaluate", "[", "list", "]"}], "}"}], "]"}], "}"}]}], ";", 
       RowBox[{"aTmp2", "=", 
        SuperscriptBox["2", "aTmp1"]}], ";", 
       RowBox[{"aTmp3", "=", 
        RowBox[{"Flatten", "[", 
         RowBox[{"Position", "[", 
          RowBox[{
           RowBox[{"aTmp4", "=", 
            RowBox[{"stateVectorFunction1", "[", "list", "]"}]}], ",", "1"}], 
          "]"}], "]"}]}], ";", 
       RowBox[{"{", 
        RowBox[{"aTmp1", ",", "aTmp2", ",", "aTmp3", ",", "aTmp4"}], 
        "}"}]}]}], "]"}], "/;", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"MemberQ", "[", 
       RowBox[{
        RowBox[{"{", "list", "}"}], ",", "up"}], "]"}], "||", 
      RowBox[{"MemberQ", "[", 
       RowBox[{
        RowBox[{"{", "list", "}"}], ",", "down"}], "]"}]}], ")"}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"SVF2", "[", "list___", "]"}], ":=", 
   RowBox[{"stateVectorFunction2", "[", "list", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "stateVectorFunction2", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "SVF2", "]"}], ";"}]], "Input"],

Cell["An example follows.", "Text"],

Cell[BoxData[
 RowBox[{"stateVectorFunction2", "[", 
  RowBox[{
   RowBox[{"upFunction", "[", "1", "]"}], ",", 
   RowBox[{"downFunction", "[", "2", "]"}], ",", 
   RowBox[{"upFunction", "[", "0", "]"}], ",", 
   RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"SVF2", "[", 
  RowBox[{
   RowBox[{"upF", "[", "1", "]"}], ",", 
   RowBox[{"downF", "[", "2", "]"}], ",", 
   RowBox[{"upF", "[", "0", "]"}], ",", 
   RowBox[{"downF", "[", "1", "]"}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
5 \[LongRightArrow] Definition of stateVectorFunction3 [list___]\
\>", "Subsubsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["stateVectorFunction3[list___] ",
  FontWeight->"Bold"],
 "function which takes as arquments a sequence of ",
 StyleBox["up ",
  FontWeight->"Bold"],
 "and/or ",
 StyleBox["down qubits ",
  FontWeight->"Bold"],
 "and gives",
 StyleBox[" ",
  FontWeight->"Bold"],
 "as result the",
 StyleBox[" decomposed array",
  FontWeight->"Bold"],
 ", the elements of which are in sequence the qubits that composes the \
general ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubit",
  FontWeight->"Bold"],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"stateVectorFunction3", ",", "SVF3"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stateVectorFunction3", "[", "listSV___", "]"}], ":=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"aTmp1", ",", "aTmp2", ",", "aTmp3", ",", "aTmp4"}], "}"}], ",", 
     RowBox[{
      RowBox[{"aTmp1", "=", "listSV"}], ";", "\[IndentingNewLine]", 
      RowBox[{"aTmp4", "=", 
       RowBox[{"{", "}"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"aTmp2", "=", 
       RowBox[{"Length", "[", "aTmp1", "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"Do", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"aTmp2", "=", 
          RowBox[{"Length", "[", "aTmp1", "]"}]}], ";", "\[IndentingNewLine]", 
         RowBox[{"aTmp3", "=", 
          RowBox[{"Part", "[", 
           RowBox[{
            RowBox[{"Position", "[", 
             RowBox[{"aTmp1", ",", "1"}], "]"}], ",", "1", ",", "1"}], 
           "]"}]}], ";", "\[IndentingNewLine]", 
         RowBox[{"If", "[", 
          RowBox[{
           RowBox[{"aTmp3", "\[LessEqual]", 
            FractionBox["aTmp2", "2"]}], ",", 
           RowBox[{
            RowBox[{"aTmp4", "=", 
             RowBox[{"Append", "[", 
              RowBox[{"aTmp4", ",", "up"}], "]"}]}], ";", 
            RowBox[{"aTmp1", "=", 
             RowBox[{"Take", "[", 
              RowBox[{"aTmp1", ",", 
               FractionBox["aTmp2", "2"]}], "]"}]}]}], ",", 
           RowBox[{
            RowBox[{"aTmp4", "=", 
             RowBox[{"Append", "[", 
              RowBox[{"aTmp4", ",", "down"}], "]"}]}], ";", 
            RowBox[{"aTmp1", "=", 
             RowBox[{"Take", "[", 
              RowBox[{"aTmp1", ",", 
               RowBox[{"-", 
                FractionBox["aTmp2", "2"]}]}], "]"}]}]}]}], "]"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", 
          RowBox[{"Log", "[", 
           RowBox[{"2", ",", "aTmp2"}], "]"}]}], "}"}]}], "]"}], ";", 
      "\[IndentingNewLine]", "aTmp4"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"SVF3", "[", "listSV___", "]"}], ":=", 
   RowBox[{"stateVectorFunction3", "[", "listSV", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"stateVectorFunction3", ",", "SVF3"}], "]"}], ";"}]], "Input"],

Cell["Some examples are the following.", "Text"],

Cell[BoxData[
 RowBox[{"stateVectorFunction3", "[", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"upFunction", "[", "2", "]"}], ",", 
    RowBox[{"downFunction", "[", "3", "]"}], ",", 
    RowBox[{"upFunction", "[", "1", "]"}]}], "]"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stateVectorFunction3", "[", 
   RowBox[{"stateVectorFunction1", "[", 
    RowBox[{
     RowBox[{"upFunction", "[", "2", "]"}], ",", 
     RowBox[{"downFunction", "[", "3", "]"}], ",", 
     RowBox[{"upFunction", "[", "1", "]"}]}], "]"}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"down", "\[Rule]", "\"\<down\>\""}], ",", 
    RowBox[{"up", "\[Rule]", "\"\<up\>\""}]}], "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"SVF3", "[", 
   RowBox[{"SVF1", "[", 
    RowBox[{
     RowBox[{"upF", "[", "2", "]"}], ",", 
     RowBox[{"downF", "[", "3", "]"}], ",", 
     RowBox[{"upF", "[", "1", "]"}]}], "]"}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"down", "\[Rule]", "\"\<down\>\""}], ",", 
    RowBox[{"up", "\[Rule]", "\"\<up\>\""}]}], "}"}]}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["3 \[LongRightArrow] Some Examples", "Subsection"],

Cell["\<\
Some examples how the previously defined functions works are the following.\
\>", "Text"],

Cell[BoxData[
 RowBox[{"downFunction", "[", "7", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"downF", "[", "7", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"upFunction", "[", "10", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"upF", "[", "10", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{"down", ",", "down", ",", "down"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"SVF1", "[", 
  RowBox[{"down", ",", "down", ",", "down"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{"up", ",", "up", ",", "down"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"SVF1", "[", 
  RowBox[{"up", ",", "up", ",", "down"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{"down", ",", "up", ",", 
   RowBox[{"downFunction", "[", "2", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"SVF1", "[", 
  RowBox[{"down", ",", "up", ",", 
   RowBox[{"downF", "[", "2", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", 
  RowBox[{
   RowBox[{"downFunction", "[", "1", "]"}], ",", 
   RowBox[{"upFunction", "[", "2", "]"}], ",", 
   RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"SVF1", "[", 
  RowBox[{
   RowBox[{"downF", "[", "1", "]"}], ",", 
   RowBox[{"upF", "[", "2", "]"}], ",", 
   RowBox[{"downF", "[", "1", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction2", "[", 
  RowBox[{
   RowBox[{"upFunction", "[", "1", "]"}], ",", 
   RowBox[{"downFunction", "[", "2", "]"}], ",", 
   RowBox[{"upFunction", "[", "0", "]"}], ",", 
   RowBox[{"downFunction", "[", "0", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"SVF2", "[", 
  RowBox[{
   RowBox[{"upF", "[", "1", "]"}], ",", 
   RowBox[{"downF", "[", "2", "]"}], ",", 
   RowBox[{"upF", "[", "0", "]"}], ",", 
   RowBox[{"downF", "[", "0", "]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction3", "[", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"upFunction", "[", "2", "]"}], ",", 
    RowBox[{"downFunction", "[", "3", "]"}], ",", 
    RowBox[{"upFunction", "[", "1", "]"}]}], "]"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"SVF3", "[", 
  RowBox[{"SVF1", "[", 
   RowBox[{
    RowBox[{"upF", "[", "2", "]"}], ",", 
    RowBox[{"downF", "[", "3", "]"}], ",", 
    RowBox[{"upF", "[", "1", "]"}]}], "]"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stateVectorFunction3", "[", 
   RowBox[{"stateVectorFunction1", "[", 
    RowBox[{
     RowBox[{"upFunction", "[", "2", "]"}], ",", 
     RowBox[{"downFunction", "[", "3", "]"}], ",", 
     RowBox[{"upFunction", "[", "10", "]"}]}], "]"}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"down", "\[Rule]", "\"\<down\>\""}], ",", 
    RowBox[{"up", "\[Rule]", "\"\<up\>\""}]}], "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"SVF3", "[", 
   RowBox[{"SVF1", "[", 
    RowBox[{
     RowBox[{"upF", "[", "2", "]"}], ",", 
     RowBox[{"downF", "[", "3", "]"}], ",", 
     RowBox[{"upF", "[", "10", "]"}]}], "]"}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"down", "\[Rule]", "\"\<down\>\""}], ",", 
    RowBox[{"up", "\[Rule]", "\"\<up\>\""}]}], "}"}]}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "5 \[LongRightArrow] Chapter 4 \[LongRightArrow]",
 StyleBox[" ",
  FontSlant->"Italic"],
 "The Quantum Functions"
}], "Section"],

Cell[CellGroupData[{

Cell["\<\
1 \[LongRightArrow] The function \[LongRightArrow] plotQCS[svFun_, {stepIn_, \
stepFin_}, {pos1_, pos2_}, opts___]\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["plotQCS[svFun_, {stepIn_, stepFin_}, {pos1_, pos2_}, opts___] ",
  FontWeight->"Bold"],
 "function which takes as argument ",
 StyleBox["m arbitrary",
  FontWeight->"Bold"],
 " ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubits",
  FontWeight->"Bold"],
 " and plots the state of them."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "plotQCS", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"plotQCS", "[", 
    RowBox[{"svFun_", ",", 
     RowBox[{"{", 
      RowBox[{"stepIn_", ",", "stepFin_"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"pos1_", ",", "pos2_"}], "}"}], ",", "opts___"}], "]"}], ":=", 
   RowBox[{"ArrayPlot", "[", 
    RowBox[{
     RowBox[{"Transpose", "@", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"Take", "[", 
         RowBox[{
          SuperscriptBox[
           RowBox[{"svFun", "[", "i", "]"}], "2"], ",", 
          RowBox[{"{", 
           RowBox[{"pos1", ",", "pos2"}], "}"}]}], "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "stepIn", ",", "stepFin"}], "}"}]}], "]"}]}], ",", 
     "opts"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "plotQCS", "]"}], ";"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] The functions \[LongRightArrow] \
ArrayFlattenOuterNoRam[stepFun_[n_], oper___]\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["ArrayFlattenOuterNoRam[stepFun_[n_], oper___]",
  FontWeight->"Bold"],
 " functions. I am not giving any further details. The only that I am saying \
is that this is a necessary help function."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "ArrayFlattenOuterNoRam", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"SetAttributes", "[", 
   RowBox[{"ArrayFlattenOuterNoRam", ",", "HoldFirst"}], "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ArrayFlattenOuterNoRam", "[", 
    RowBox[{
     RowBox[{"stepFun_", "[", "n_", "]"}], ",", "oper___"}], "]"}], ":=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"aTmp1", ",", "aTmp2", ",", "aTmp3"}], "}"}], ",", 
     RowBox[{
      RowBox[{"aTmp1", "=", 
       RowBox[{"{", "oper", "}"}]}], ";", 
      RowBox[{"aTmp2", "=", 
       RowBox[{"Flatten", "[", 
        RowBox[{
         RowBox[{"Outer", "[", 
          RowBox[{"List", ",", 
           RowBox[{"Apply", "[", 
            RowBox[{"Sequence", ",", 
             RowBox[{"Range", "@", 
              RowBox[{"Map", "[", 
               RowBox[{"Length", ",", "aTmp1"}], "]"}]}]}], "]"}]}], "]"}], 
         ",", 
         RowBox[{
          RowBox[{"Length", "@", "aTmp1"}], "-", "1"}]}], "]"}]}], ";", 
      RowBox[{"aTmp3", "=", 
       RowBox[{"Length", "@", "aTmp2"}]}], ";", 
      RowBox[{
       RowBox[{"stepFun", "[", 
        RowBox[{"n", "+", "1"}], "]"}], "=", 
       RowBox[{"Table", "[", 
        RowBox[{
         RowBox[{"Dot", "[", 
          RowBox[{
           RowBox[{"Flatten", "[", 
            RowBox[{"Outer", "[", 
             RowBox[{"Times", ",", 
              RowBox[{"Apply", "[", 
               RowBox[{"Sequence", ",", 
                RowBox[{"Table", "[", 
                 RowBox[{
                  RowBox[{"Part", "[", 
                   RowBox[{"aTmp1", ",", "i", ",", 
                    RowBox[{"Part", "[", 
                    RowBox[{"aTmp2", ",", "j", ",", "i"}], "]"}]}], "]"}], 
                  ",", 
                  RowBox[{"{", 
                   RowBox[{"i", ",", "1", ",", 
                    RowBox[{"Length", "@", "aTmp1"}]}], "}"}]}], "]"}]}], 
               "]"}]}], "]"}], "]"}], ",", 
           RowBox[{"stepFun", "[", "n", "]"}]}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"j", ",", "1", ",", "aTmp3"}], "}"}]}], "]"}]}]}]}], 
    "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "ArrayFlattenOuterNoRam", "]"}], ";"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "3 \[LongRightArrow] The function \[LongRightArrow]  \
GeneralArrayFlattenOuterNoRam[",
 StyleBox["stepFun_, OpT_",
  FontWeight->"Bold"],
 "]"
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["GeneralArrayFlattenOuterNoRam[stepFun_, OpT_] ",
  FontWeight->"Bold"],
 "function. The purpose of this function is to build - having the quantum \
operators and the initial n-qubit - the rest n-qubits."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "GeneralArrayFlattenOuterNoRam", "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFun_", ",", "OpT_"}], "]"}], ":=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", "aTmp1", "}"}], ",", 
     RowBox[{
      RowBox[{"aTmp1", "=", 
       RowBox[{"Length", "@", "OpT"}]}], ";", 
      RowBox[{"Join", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"stepFun", "[", "1", "]"}], "}"}], ",", 
        RowBox[{"Table", "[", 
         RowBox[{
          RowBox[{"ArrayFlattenOuterNoRam", "[", 
           RowBox[{
            RowBox[{"stepFun", "[", "i", "]"}], ",", 
            RowBox[{"Apply", "[", 
             RowBox[{"Sequence", ",", 
              RowBox[{"Part", "[", 
               RowBox[{"OpT", ",", 
                RowBox[{"aTmp1", "-", "i", "+", "1"}]}], "]"}]}], "]"}]}], 
           "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"i", ",", "1", ",", "aTmp1"}], "}"}]}], "]"}]}], "]"}]}]}],
     "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "GeneralArrayFlattenOuterNoRam", "]"}], 
  ";"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "6 \[LongRightArrow] Chapter 5 \[LongRightArrow]",
 StyleBox[" ",
  FontSlant->"Italic"],
 "Some Examples Of Quantum Operations"
}], "Section"],

Cell[CellGroupData[{

Cell["\<\
1 \[LongRightArrow] Some Typical Examples of Quantum Operations\
\>", "Subsection"],

Cell[CellGroupData[{

Cell["1 \[LongRightArrow] Example", "Subsubsection"],

Cell[TextData[{
 "Here I evaluate the state vector ",
 StyleBox["\[LongRightArrow] ( \[CapitalEta] \[CircleTimes] \[CapitalIota] ) \
\[CircleTimes] ( CNOT ) \[CircleTimes] ( CNOT ) \[CircleTimes] ( \
\[CapitalEta] \[CircleTimes] \[CapitalIota] ) \[CircleTimes] \
\[VerticalSeparator] 1 0 \[RightAngleBracket]",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell["This is the initial 2-qubit.", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stepFunction", "[", "1", "]"}], "=", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"downFunction", "[", "1", "]"}], ",", 
    RowBox[{"upFunction", "[", "1", "]"}]}], "]"}]}]], "Input"],

Cell["\<\
This is the sequence of operators that act upon the initial qubit.\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", "qCN", "}"}], ",", 
     RowBox[{"{", "qCN", "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[TextData[{
 "This is the array which holds as elements ",
 StyleBox["all the 2",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubits.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
  RowBox[{"stepFunction", ",", "OpT"}], "]"}]], "Input"],

Cell[TextData[{
 "Here is the plot corresponding to the above Matrix - array of arrays. ",
 StyleBox["I denote with the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Red color the exact zero's and with the Black color the exact \
one's.",
  FontWeight->"Bold"],
 " "
}], "Text"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Black"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["2 \[LongRightArrow] Example", "Subsubsection"],

Cell[TextData[{
 "Here I evaluate the state vector ",
 StyleBox["\[LongRightArrow] ",
  FontWeight->"Bold"],
 " ",
 StyleBox["( I \[CircleTimes] H ) \[CircleTimes] ( \[CapitalEta] \
\[CircleTimes] \[CapitalIota] ) \[CircleTimes] ( CNOT ) \[CircleTimes] ( \
\[CapitalEta] \[CircleTimes] H ) \[CircleTimes] \[VerticalSeparator] 0 1 \
\[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stepFunction", "[", "1", "]"}], "=", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"upFunction", "[", "1", "]"}], ",", 
    RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "OpT", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qH"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", "qCN", "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qH"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
  RowBox[{"stepFunction", ",", "OpT"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "[", "%", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Black"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["3 \[LongRightArrow] Example", "Subsubsection"],

Cell[TextData[{
 "Here I evaluate the state vector ",
 StyleBox["\[LongRightArrow] ",
  FontWeight->"Bold"],
 " ",
 StyleBox[" ( CNOT ) \[CircleTimes] ( \[CapitalEta] \[CircleTimes] I ) \
\[CircleTimes] \[VerticalSeparator] 0 0 \[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stepFunction", "[", "1", "]"}], "=", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"upFunction", "[", "2", "]"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "OpT", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", "qCN", "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
  RowBox[{"stepFunction", ",", "OpT"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "[", "%", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Black"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["4 \[LongRightArrow] Example", "Subsubsection"],

Cell[TextData[{
 "Here I evaluate the state vector ",
 StyleBox["\[LongRightArrow] ",
  FontWeight->"Bold"],
 " ",
 StyleBox[" ( I \[CircleTimes] CNOT ) \[CircleTimes] ( CNOT ",
  FontWeight->"Bold"],
 Cell[BoxData[
  FormBox[
   StyleBox["\[CirclePlus]",
    FontWeight->"Bold"], TraditionalForm]],
  FontWeight->"Bold"],
 StyleBox[" I ) \[CircleTimes] ( \[CapitalEta] \[CircleTimes] I \
\[CircleTimes] I ) \[CircleTimes] \[VerticalSeparator] 0 0 0 \
\[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stepFunction", "[", "1", "]"}], "=", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"upFunction", "[", "3", "]"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "OpT", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qCN", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI", ",", "qI"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
  RowBox[{"stepFunction", ",", "OpT"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Black"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["5 \[LongRightArrow] Example", "Subsubsection"],

Cell[TextData[StyleBox["Variation 1.",
 FontWeight->"Bold"]], "Text"],

Cell[TextData[{
 "Here I act with the operator ",
 StyleBox["( I \[CircleTimes] CNOT ) \[CircleTimes] ( CNOT \[CircleTimes] I ) \
\[CircleTimes] ( \[CapitalEta] \[CircleTimes] I \[CircleTimes] I ) ",
  FontWeight->"Bold"],
 "several times and I observe the evolution of the quantum state vector. The \
initial vector is the  ",
 StyleBox["\[VerticalSeparator] 0 1 1 \[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stepFunction", "[", "1", "]"}], "=", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"upFunction", "[", "1", "]"}], ",", 
    RowBox[{"downFunction", "[", "2", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qCN", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT2", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{"OpT1", ",", 
       RowBox[{"{", "200", "}"}]}], "]"}], ",", "1"}], "]"}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"StepsDenseNoRam", "=", 
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "@", "StepsDenseNoRam"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "1"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"20", ",", "240"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "5"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"40", ",", "140"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "12"}], "}"}]}]}], "]"}]], "Input"],

Cell[TextData[StyleBox["Variation 2.",
 FontWeight->"Bold"]], "Text"],

Cell[TextData[{
 "Here I act with each of the operators ",
 StyleBox["{ ( I \[CircleTimes] CNOT ) , ( CNOT \[CircleTimes] I ) , ( \
\[CapitalEta] \[CircleTimes] I \[CircleTimes] I ) } ",
  FontWeight->"Bold"],
 "in random order and I observe the evolution of the quantum state vector. \
The initial quantum state vector is the ",
 StyleBox["\[VerticalSeparator] 0 1 1 \[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stepFunction", "[", "1", "]"}], "=", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"upFunction", "[", "1", "]"}], ",", 
    RowBox[{"downFunction", "[", "2", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qCN", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"OpT2", "=", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"Part", "[", 
        RowBox[{"OpT1", ",", 
         RowBox[{"RandomInteger", "[", 
          RowBox[{"{", 
           RowBox[{"1", ",", 
            RowBox[{"Length", "[", "OpT1", "]"}]}], "}"}], "]"}]}], "]"}], 
       ",", 
       RowBox[{"{", 
        SuperscriptBox["10", "3"], "}"}]}], "]"}]}], ";"}], ")"}], "//", 
  "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"StepsDenseNoRam", "=", 
     RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
      RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}], ")"}], "//", 
  "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "@", "StepsDenseNoRam"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"40", ",", "1"}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["6 \[LongRightArrow] Example", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stepFunction", "[", "1", "]"}], "=", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"upFunction", "[", "4", "]"}], ",", 
    RowBox[{"downFunction", "[", "0", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"RR", "[", "n_", "]"}], ":=", 
  RowBox[{"RotateRight", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"qI", ",", "qCN", ",", "qH"}], "}"}], ",", "n"}], 
   "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"RR", "[", "i", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "1", ",", "400"}], "}"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT2", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{"OpT1", ",", 
       RowBox[{"{", "1", "}"}]}], "]"}], ",", "1"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"StepsDenseNoRam", "=", 
    RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
     RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "@", "StepsDenseNoRam"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "3"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"20", ",", "240"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "5"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"40", ",", "140"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "12"}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["7 \[LongRightArrow] Example", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stepFunction", "[", "1", "]"}], "=", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"upFunction", "[", "1", "]"}], ",", 
    RowBox[{"downFunction", "[", "1", "]"}], ",", 
    RowBox[{"upFunction", "[", "1", "]"}], ",", 
    RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"RR", "[", "n_", "]"}], ":=", 
  RowBox[{"RotateRight", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"qI", ",", "qCN", ",", "qH"}], "}"}], ",", "n"}], 
   "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"RR", "[", "i", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "1", ",", "400"}], "}"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT2", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{"OpT1", ",", 
       RowBox[{"{", "1", "}"}]}], "]"}], ",", "1"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"StepsDenseNoRam", "=", 
    RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
     RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "@", "StepsDenseNoRam"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "3"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"20", ",", "240"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "5"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"40", ",", "140"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "12"}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["8 \[LongRightArrow] Example", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stepFunction", "[", "1", "]"}], "=", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"upFunction", "[", "5", "]"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"RR", "[", "n_", "]"}], ":=", 
   RowBox[{"RotateRight", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN", ",", "qH", ",", "qH"}], "}"}], ",", "n"}], 
    "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"RR", "[", "i", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "1", ",", "100"}], "}"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT2", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{"OpT1", ",", 
       RowBox[{"{", "1", "}"}]}], "]"}], ",", "1"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"StepsDenseNoRam", "=", 
    RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
     RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "@", "StepsDenseNoRam"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "5"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"10", ",", "10"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"73", ",", "98"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "5"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"10", ",", "10"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"48", ",", "98"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "5"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"10", ",", "10"}], "}"}]}]}], "]"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] Testing the Efficiency of the Dense Matrices Techniques\
\>", "Subsection"],

Cell[CellGroupData[{

Cell["1 \[LongRightArrow] Example \[LongRightArrow] Sparse Matrices", \
"Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "@", "stepFunction"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numBits", "=", "12"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numOne", "=", 
   FractionBox["numBits", "3"]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepFunction", "[", "1", "]"}], "=", 
   RowBox[{"stateVectorFunction1", "[", 
    RowBox[{"upFunction", "[", "numBits", "]"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "@", 
  RowBox[{"stepFunction", "[", "1", "]"}]}]], "Input"],

Cell[BoxData[
 FractionBox[
  RowBox[{"ByteCount", "@", 
   RowBox[{"stepFunction", "[", "1", "]"}]}], 
  SuperscriptBox["1024.00", "2"]]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", "qI"}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", "qCN"}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", "qH"}]}]}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qI", ",", "qCN"}], "]"}]}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qCN", ",", "qI"}], "]"}]}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qH", ",", "qI", ",", "qI"}], "]"}]}]}]}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada1", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qI", ",", "qCN"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada2", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qCN", ",", "qI"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada3", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qH", ",", "qI", ",", "qI"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada1", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada2", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada3", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"NotebookSave", "[", "]"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] Example \[LongRightArrow] Sparse & Dense Matrices\
\>", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "@", "stepFunction"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numBits", "=", "12"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numOne", "=", 
   FractionBox["numBits", "3"]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepFunction", "[", "1", "]"}], "=", 
   RowBox[{"stateVectorFunction1", "[", 
    RowBox[{"upFunction", "[", "numBits", "]"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "@", 
  RowBox[{"stepFunction", "[", "1", "]"}]}]], "Input"],

Cell[BoxData[
 FractionBox[
  RowBox[{"ByteCount", "@", 
   RowBox[{"stepFunction", "[", "1", "]"}]}], 
  SuperscriptBox["1024.00", "2"]]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qI", ",", "qI", ",", "qH"}], "]"}]}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qI", ",", "qH", ",", "qH"}], "]"}]}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qH", ",", "qH", ",", "qH"}], "]"}]}]}]}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada1", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qI", ",", "qI", ",", "qH"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada2", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qI", ",", "qH", ",", "qH"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada3", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qH", ",", "qH", ",", "qH"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada1", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada2", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada3", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"NotebookSave", "[", "]"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
3 \[LongRightArrow] Example \[LongRightArrow] Only Dense Matrices\
\>", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "@", "stepFunction"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numBits", "=", "12"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numOne", "=", 
   FractionBox["numBits", "3"]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepFunction", "[", "1", "]"}], "=", 
   RowBox[{"stateVectorFunction1", "[", 
    RowBox[{"upFunction", "[", "numBits", "]"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "@", 
  RowBox[{"stepFunction", "[", "1", "]"}]}]], "Input"],

Cell[BoxData[
 FractionBox[
  RowBox[{"ByteCount", "@", 
   RowBox[{"stepFunction", "[", "1", "]"}]}], 
  SuperscriptBox["1024.00", "2"]]], "Input"],

Cell[BoxData[
 RowBox[{"ArrayPlot", "@", 
  RowBox[{"Abs", "@", 
   RowBox[{"KroneckerProduct", "[", 
    RowBox[{"qH", ",", "qH", ",", "qH"}], "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada1", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qH", ",", "qH", ",", "qH"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada2", "=", "omada1"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada1", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada2", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"NotebookSave", "[", "]"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
7 \[LongRightArrow] Chapter 6 \[LongRightArrow] A Realistic Example \
\[LongRightArrow] Grover's Algorithm\
\>", "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 "1 \[LongRightArrow] Here I make a graphical demonstration of the ",
 StyleBox["Grover's Algorithm",
  FontWeight->"Bold",
  FontSlant->"Italic"]
}], "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"DownQubits", "=", "0"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"UpQubits", "=", "8"}], ";"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 1. ",
  FontWeight->"Bold"],
 "Here I define the ",
 StyleBox["stepF[1]",
  FontWeight->"Bold"],
 " initial state of the quantum register. I made this in an arbitrary way.  "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepF", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepF", "[", "1", "]"}], "=", 
   RowBox[{"N", "@", 
    RowBox[{"SVF1", "[", 
     RowBox[{
      RowBox[{"downF", "@", "DownQubits"}], ",", 
      RowBox[{"upF", "@", "UpQubits"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 2. ",
  FontWeight->"Bold"],
 "Here I define the ",
 StyleBox["eigenvector ",
  FontWeight->"Bold"],
 "that I want finally to produce."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "WILF", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"WILF", "=", 
   RowBox[{"N", "@", 
    RowBox[{"SVF1", "[", 
     RowBox[{"upF", "[", 
      RowBox[{"UpQubits", "+", "DownQubits"}], "]"}], "]"}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "WILF", "]"}], ";"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 3. ",
  FontWeight->"Bold"],
 "Here I make the ",
 StyleBox["first two steps.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OperatorsTable", "=", 
   RowBox[{"N", "[", 
    RowBox[{"{", 
     RowBox[{"Table", "[", 
      RowBox[{"qH", ",", 
       RowBox[{"{", 
        RowBox[{"DownQubits", "+", "UpQubits"}], "}"}]}], "]"}], "}"}], 
    "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"GeneralArrayFlattenOuterNoRam", "[", 
    RowBox[{"stepF", ",", "OperatorsTable"}], "]"}], ";"}], "]"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 4. ",
  FontWeight->"Bold"],
 "Here I define the ",
 Cell[BoxData[
  FormBox[
   OverscriptBox["O", "^"], TraditionalForm]],
  FontWeight->"Bold",
  FontSlant->"Italic"],
 " and the ",
 Cell[BoxData[
  FormBox[
   StyleBox[
    OverscriptBox["S", "^"],
    FontWeight->"Bold"], TraditionalForm]]],
 StyleBox[".",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"OOp", ",", "GOp", ",", "bothGrover"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"OOp", "=", 
    RowBox[{"N", "[", 
     RowBox[{
      RowBox[{"IdentityMatrix", "[", 
       SuperscriptBox["2", 
        RowBox[{"DownQubits", "+", "UpQubits"}]], "]"}], "-", 
      RowBox[{"2", 
       RowBox[{"Dot", "[", 
        RowBox[{
         RowBox[{"Transpose", "[", 
          RowBox[{"{", "WILF", "}"}], "]"}], ",", 
         RowBox[{"{", "WILF", "}"}]}], "]"}]}]}], "]"}]}], ";"}], 
  "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"GOp", "=", 
    RowBox[{"N", "[", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{"IdentityMatrix", "[", 
        SuperscriptBox["2", 
         RowBox[{"DownQubits", "+", "UpQubits"}]], "]"}]}], "+", 
      RowBox[{"2", 
       RowBox[{"Dot", "[", 
        RowBox[{
         RowBox[{"Transpose", "[", 
          RowBox[{"{", 
           RowBox[{"stepF", "[", "2", "]"}], "}"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"stepF", "[", "2", "]"}], "}"}]}], "]"}]}]}], "]"}]}], 
   ";"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"bothGrover", "=", 
    RowBox[{"Dot", "[", 
     RowBox[{"GOp", ",", "OOp"}], "]"}]}], ";"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"OOp", ",", "GOp", ",", "bothGrover"}], "]"}], ";"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 5. Now",
  FontWeight->"Bold"],
 " the ",
 StyleBox["Grover's Algorithm",
  FontWeight->"Bold",
  FontSlant->"Italic"],
 " starts."
}], "Text"],

Cell[BoxData[
 RowBox[{"periodConstant", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      FractionBox["Pi", "4"], "*", 
      SqrtBox[
       SuperscriptBox["2", 
        RowBox[{"UpQubits", "+", "DownQubits"}]]]}], "-", 
     FractionBox["1", "2"]}], ")"}], "//", "N"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepF", "[", "n_", "]"}], ":=", 
   RowBox[{
    RowBox[{"stepF", "[", "n", "]"}], "=", 
    RowBox[{"Dot", "[", 
     RowBox[{"bothGrover", ",", 
      RowBox[{"stepF", "[", 
       RowBox[{"n", "-", "1"}], "]"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepF", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     RowBox[{"2", "*", "2", "*", 
      RowBox[{"Ceiling", "[", "periodConstant", "]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "10"}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1.", "\[Rule]", "Green"}], ",", 
      RowBox[{"0.", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["8 \[LongRightArrow] Bibliography", "Section"],

Cell[TextData[{
 StyleBox["1.",
  FontWeight->"Bold"],
 " Quantum Computer Science: An Introduction. N. David Mermin. Cambridge \
University Press (2007)."
}], "Text"],

Cell[TextData[{
 StyleBox["2.",
  FontWeight->"Bold"],
 " Mathematics of Quantum Computation. Ranee K. Brylinski, Goong Chen. ",
 "Chapman & Hall (2002)."
}], "Text"],

Cell[TextData[{
 StyleBox["3.",
  FontWeight->"Bold"],
 " Principles of Quantum Computation and Information: Basic Concepts Vol 1. \
Giuliano Benenti, Giulio Casati, Giuliano Strini.\nWorld Scientific \
Publishing, (2004)."
}], "Text"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1596, 801},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
ShowCellBracket->False,
ShowSelection->True,
Deployed->True,
CellContext->Notebook,
TrackCellChangeTimes->False,
Magnification->1.,
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> 
     FrontEnd`FileName[{"Creative"}, "NaturalColor.nb", CharacterEncoding -> 
       "WindowsGreek"]]], 
   Cell[
    StyleData["Title"], FontFamily -> "Times New Roman", FontSize -> 36, 
    FontWeight -> "Bold", FontSlant -> "Plain", 
    FontVariations -> {"StrikeThrough" -> False, "Underline" -> False}, 
    FontColor -> GrayLevel[0], Background -> RGBColor[1, 0, 0]], 
   Cell[
    StyleData["Section"], FontFamily -> "Times New Roman", FontSize -> 34, 
    FontWeight -> "Bold", FontSlant -> "Plain", 
    FontVariations -> {"StrikeThrough" -> False, "Underline" -> False}, 
    FontColor -> GrayLevel[0], Background -> 
    RGBColor[0.49019607843137253`, 0.8, 0.8666666666666667]], 
   Cell[
    StyleData["Subsection"], CellDingbat -> None, FontFamily -> 
    "Times New Roman", FontSize -> 28, FontWeight -> "Bold", FontSlant -> 
    "Plain", FontVariations -> {
     "StrikeThrough" -> False, "Underline" -> False}, FontColor -> 
    GrayLevel[0], Background -> 
    RGBColor[0.6823529411764706, 0.8235294117647058, 0.41568627450980394`]], 
   Cell[
    StyleData["Input"], FontFamily -> "Courier New", FontSize -> 26, 
    Background -> 
    RGBColor[0.9882352941176471, 0.6784313725490196, 0.2196078431372549]], 
   Cell[
    StyleData["Output"], FontFamily -> "Courier New", FontSize -> 26, 
    FontColor -> GrayLevel[0], Background -> GrayLevel[1]], 
   Cell[
    StyleData["Text"], TextJustification -> 1., FontFamily -> 
    "Times New Roman", FontSize -> 26, FontWeight -> "Plain", FontSlant -> 
    "Plain", 
    FontVariations -> {"StrikeThrough" -> False, "Underline" -> False}, 
    FontColor -> GrayLevel[0], Background -> 
    RGBColor[0.9607843137254902, 0.8196078431372549, 0.996078431372549]], 
   Cell[
    StyleData["DisplayFormulaNumbered"], FontFamily -> "Times New Roman", 
    FontSize -> 26, FontWeight -> "Bold", FontSlant -> "Plain", 
    FontVariations -> {"StrikeThrough" -> False, "Underline" -> False}, 
    FontColor -> GrayLevel[0], Background -> 
    RGBColor[0.796078431372549, 0.803921568627451, 0.996078431372549]], 
   Cell[
    StyleData["Subsubsection"], CellDingbat -> None, FontFamily -> 
    "Times New Roman", FontSize -> 26, FontWeight -> "Bold", FontSlant -> 
    "Plain", FontVariations -> {
     "StrikeThrough" -> False, "Underline" -> False}, FontColor -> 
    GrayLevel[0], Background -> RGBColor[1, 0.5, 0.5]]}, 
  WindowSize -> {708, 681}, WindowMargins -> {{0, Automatic}, {Automatic, 0}},
   FrontEndVersion -> "9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
   StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[1463, 33, 52, 1, 55, "Input"],
Cell[CellGroupData[{
Cell[1540, 38, 30, 0, 87, "Title"],
Cell[CellGroupData[{
Cell[1595, 42, 51, 0, 92, "Section"],
Cell[CellGroupData[{
Cell[1671, 46, 53, 0, 68, "Subsection"],
Cell[1727, 48, 1050, 29, 223, "Text"],
Cell[2780, 79, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[2873, 86, 150, 5, 93, "Section"],
Cell[CellGroupData[{
Cell[3048, 95, 125, 5, 68, "Subsection"],
Cell[3176, 102, 478, 23, 93, "Text"],
Cell[3657, 127, 83, 2, 55, "Input"],
Cell[3743, 131, 110, 3, 55, "Input"],
Cell[3856, 136, 109, 3, 55, "Input"],
Cell[3968, 141, 108, 3, 55, "Input"],
Cell[4079, 146, 505, 17, 83, "Input"],
Cell[4587, 165, 82, 2, 55, "Input"],
Cell[4672, 169, 149, 5, 55, "Input"],
Cell[4824, 176, 259, 13, 60, "Text"],
Cell[5086, 191, 1133, 33, 112, "DisplayFormulaNumbered"],
Cell[6222, 226, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[6303, 232, 98, 2, 68, "Subsection"],
Cell[6404, 236, 555, 26, 93, "Text"],
Cell[6962, 264, 96, 2, 55, "Input"],
Cell[7061, 268, 843, 24, 170, "Input"],
Cell[7907, 294, 95, 2, 55, "Input"],
Cell[8005, 298, 366, 19, 60, "Text"],
Cell[8374, 319, 188, 6, 55, "Input"],
Cell[8565, 327, 367, 19, 60, "Text"],
Cell[8935, 348, 188, 6, 55, "Input"],
Cell[9126, 356, 367, 19, 60, "Text"],
Cell[9496, 377, 188, 6, 55, "Input"],
Cell[9687, 385, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9780, 392, 96, 2, 92, "Section"],
Cell[CellGroupData[{
Cell[9901, 398, 118, 3, 68, "Subsection"],
Cell[10022, 403, 539, 22, 93, "Text"],
Cell[10564, 427, 102, 2, 55, "Input"],
Cell[10669, 431, 83, 2, 55, "Input"],
Cell[10755, 435, 124, 3, 55, "Input"],
Cell[10882, 440, 123, 3, 55, "Input"],
Cell[11008, 445, 122, 3, 55, "Input"],
Cell[11133, 450, 2253, 68, 310, "Input"],
Cell[13389, 520, 247, 6, 55, "Input"],
Cell[13639, 528, 101, 2, 55, "Input"],
Cell[13743, 532, 82, 2, 55, "Input"],
Cell[13828, 536, 319, 9, 55, "Input"],
Cell[14150, 547, 323, 16, 60, "Text"],
Cell[14476, 565, 2319, 64, 122, "DisplayFormulaNumbered"],
Cell[16798, 631, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[16879, 637, 89, 2, 68, "Subsection"],
Cell[16971, 641, 122, 4, 60, "Text"],
Cell[17096, 647, 97, 2, 55, "Input"],
Cell[17196, 651, 80, 2, 55, "Input"],
Cell[17279, 655, 461, 13, 55, "Input"],
Cell[17743, 670, 96, 2, 55, "Input"],
Cell[17842, 674, 79, 2, 55, "Input"],
Cell[17924, 678, 60, 1, 55, "Input"],
Cell[17987, 681, 77, 1, 55, "Input"],
Cell[18067, 684, 261, 13, 60, "Text"],
Cell[18331, 699, 652, 18, 82, "DisplayFormulaNumbered"],
Cell[18986, 719, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[19067, 725, 147, 4, 68, "Subsection"],
Cell[19217, 731, 136, 4, 60, "Text"],
Cell[19356, 737, 98, 2, 55, "Input"],
Cell[19457, 741, 81, 2, 55, "Input"],
Cell[19541, 745, 552, 17, 83, "Input"],
Cell[20096, 764, 152, 4, 55, "Input"],
Cell[20251, 770, 97, 2, 55, "Input"],
Cell[20351, 774, 80, 2, 55, "Input"],
Cell[20434, 778, 82, 2, 55, "Input"],
Cell[20519, 782, 81, 2, 55, "Input"],
Cell[20603, 786, 80, 2, 55, "Input"],
Cell[20686, 790, 91, 2, 55, "Input"],
Cell[20780, 794, 267, 13, 60, "Text"],
Cell[21050, 809, 801, 22, 88, "DisplayFormulaNumbered"],
Cell[21854, 833, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[21935, 839, 156, 5, 68, "Subsection"],
Cell[22094, 846, 121, 4, 60, "Text"],
Cell[22218, 852, 80, 2, 55, "Input"],
Cell[22301, 856, 96, 2, 55, "Input"],
Cell[22400, 860, 542, 16, 83, "Input"],
Cell[22945, 878, 79, 2, 55, "Input"],
Cell[23027, 882, 95, 2, 55, "Input"],
Cell[23125, 886, 60, 1, 55, "Input"],
Cell[23188, 889, 76, 1, 55, "Input"],
Cell[23267, 892, 249, 13, 60, "Text"],
Cell[23519, 907, 917, 28, 126, "DisplayFormulaNumbered"],
Cell[24439, 937, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[24520, 943, 138, 4, 68, "Subsection"],
Cell[24661, 949, 127, 4, 60, "Text"],
Cell[24791, 955, 81, 2, 55, "Input"],
Cell[24875, 959, 101, 2, 55, "Input"],
Cell[24979, 963, 489, 14, 55, "Input"],
Cell[25471, 979, 80, 2, 55, "Input"],
Cell[25554, 983, 100, 2, 55, "Input"],
Cell[25657, 987, 61, 1, 55, "Input"],
Cell[25721, 990, 81, 1, 55, "Input"],
Cell[25805, 993, 254, 13, 60, "Text"],
Cell[26062, 1008, 871, 24, 160, "DisplayFormulaNumbered"],
Cell[26936, 1034, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[27017, 1040, 116, 3, 68, "Subsection"],
Cell[27136, 1045, 147, 4, 60, "Text"],
Cell[27286, 1051, 82, 2, 55, "Input"],
Cell[27371, 1055, 111, 3, 55, "Input"],
Cell[27485, 1060, 82, 2, 55, "Input"],
Cell[27570, 1064, 81, 2, 55, "Input"],
Cell[27654, 1068, 80, 2, 55, "Input"],
Cell[27737, 1072, 530, 15, 55, "Input"],
Cell[28270, 1089, 163, 4, 55, "Input"],
Cell[28436, 1095, 81, 2, 55, "Input"],
Cell[28520, 1099, 110, 3, 55, "Input"],
Cell[28633, 1104, 92, 2, 55, "Input"],
Cell[28728, 1108, 121, 3, 55, "Input"],
Cell[28852, 1113, 266, 13, 60, "Text"],
Cell[29121, 1128, 1102, 30, 165, "DisplayFormulaNumbered"],
Cell[30226, 1160, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[30307, 1166, 105, 2, 68, "Subsection"],
Cell[30415, 1170, 138, 4, 60, "Text"],
Cell[30556, 1176, 82, 2, 55, "Input"],
Cell[30641, 1180, 114, 3, 55, "Input"],
Cell[30758, 1185, 1293, 37, 170, "Input"],
Cell[32054, 1224, 81, 2, 55, "Input"],
Cell[32138, 1228, 113, 3, 55, "Input"],
Cell[32254, 1233, 62, 1, 55, "Input"],
Cell[32319, 1236, 91, 1, 55, "Input"],
Cell[32413, 1239, 264, 13, 60, "Text"],
Cell[32680, 1254, 1169, 28, 280, "DisplayFormulaNumbered"]
}, Open  ]],
Cell[CellGroupData[{
Cell[33886, 1287, 79, 0, 68, "Subsection"],
Cell[33968, 1289, 120, 4, 60, "Text"],
Cell[34091, 1295, 80, 2, 55, "Input"],
Cell[34174, 1299, 95, 2, 55, "Input"],
Cell[34272, 1303, 1275, 37, 132, "Input"],
Cell[35550, 1342, 79, 2, 55, "Input"],
Cell[35632, 1346, 94, 2, 55, "Input"],
Cell[35729, 1350, 60, 1, 55, "Input"],
Cell[35792, 1353, 75, 1, 55, "Input"],
Cell[35870, 1356, 206, 10, 60, "Text"],
Cell[36079, 1368, 1143, 28, 304, "DisplayFormulaNumbered"],
Cell[37225, 1398, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[37318, 1405, 200, 6, 92, "Section"],
Cell[CellGroupData[{
Cell[37543, 1415, 95, 2, 68, "Subsection"],
Cell[37641, 1419, 152, 8, 60, "Text"],
Cell[37796, 1429, 107, 3, 55, "Input"],
Cell[37906, 1434, 117, 4, 55, "Input"],
Cell[38026, 1440, 119, 4, 55, "Input"],
Cell[38148, 1446, 106, 3, 55, "Input"],
Cell[38257, 1451, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[38338, 1457, 90, 2, 68, "Subsection"],
Cell[CellGroupData[{
Cell[38453, 1463, 194, 7, 58, "Subsubsection"],
Cell[38650, 1472, 369, 14, 92, "Text"],
Cell[39022, 1488, 125, 3, 55, "Input"],
Cell[39150, 1493, 1939, 53, 284, "Input"],
Cell[41092, 1548, 157, 4, 55, "Input"],
Cell[41252, 1554, 97, 2, 55, "Input"],
Cell[41352, 1558, 81, 2, 55, "Input"],
Cell[41436, 1562, 48, 0, 59, "Text"],
Cell[41487, 1564, 76, 1, 55, "Input"],
Cell[41566, 1567, 111, 2, 55, "Input"],
Cell[41680, 1571, 124, 2, 55, "Input"],
Cell[41807, 1575, 140, 3, 55, "Input"],
Cell[41950, 1580, 124, 3, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[42111, 1588, 204, 8, 58, "Subsubsection"],
Cell[42318, 1598, 467, 20, 93, "Text"],
Cell[42788, 1620, 153, 4, 55, "Input"],
Cell[42944, 1626, 263, 8, 55, "Input"],
Cell[43210, 1636, 267, 8, 55, "Input"],
Cell[43480, 1646, 138, 4, 55, "Input"],
Cell[43621, 1652, 142, 4, 55, "Input"],
Cell[43766, 1658, 152, 4, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[43955, 1667, 58, 0, 58, "Subsubsection"],
Cell[44016, 1669, 65, 1, 55, "Input"],
Cell[44084, 1672, 58, 1, 55, "Input"],
Cell[44145, 1675, 63, 1, 55, "Input"],
Cell[44211, 1678, 56, 1, 55, "Input"],
Cell[44270, 1681, 120, 3, 55, "Input"],
Cell[44393, 1686, 76, 1, 55, "Input"],
Cell[44472, 1689, 98, 2, 55, "Input"],
Cell[44573, 1693, 150, 3, 55, "Input"],
Cell[44726, 1698, 224, 5, 55, "Input"],
Cell[44953, 1705, 187, 5, 55, "Input"],
Cell[45143, 1712, 420, 11, 94, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[45600, 1728, 96, 2, 58, "Subsubsection"],
Cell[45699, 1732, 895, 41, 126, "Text"],
Cell[46597, 1775, 125, 3, 55, "Input"],
Cell[46725, 1780, 1206, 36, 177, "Input"],
Cell[47934, 1818, 157, 4, 55, "Input"],
Cell[48094, 1824, 97, 2, 55, "Input"],
Cell[48194, 1828, 81, 2, 55, "Input"],
Cell[48278, 1832, 35, 0, 59, "Text"],
Cell[48316, 1834, 272, 6, 55, "Input"],
Cell[48591, 1842, 228, 6, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[48856, 1853, 97, 2, 58, "Subsubsection"],
Cell[48956, 1857, 570, 24, 93, "Text"],
Cell[49529, 1883, 125, 3, 55, "Input"],
Cell[49657, 1888, 1982, 50, 435, "Input"],
Cell[51642, 1940, 161, 4, 55, "Input"],
Cell[51806, 1946, 124, 3, 55, "Input"],
Cell[51933, 1951, 48, 0, 59, "Text"],
Cell[51984, 1953, 273, 6, 55, "Input"],
Cell[52260, 1961, 435, 11, 94, "Input"],
Cell[52698, 1974, 382, 11, 55, "Input"],
Cell[53083, 1987, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[53176, 1994, 55, 0, 68, "Subsection"],
Cell[53234, 1996, 99, 2, 59, "Text"],
Cell[53336, 2000, 65, 1, 55, "Input"],
Cell[53404, 2003, 58, 1, 55, "Input"],
Cell[53465, 2006, 64, 1, 55, "Input"],
Cell[53532, 2009, 57, 1, 55, "Input"],
Cell[53592, 2012, 115, 2, 55, "Input"],
Cell[53710, 2016, 99, 2, 55, "Input"],
Cell[53812, 2020, 111, 2, 55, "Input"],
Cell[53926, 2024, 95, 2, 55, "Input"],
Cell[54024, 2028, 150, 3, 55, "Input"],
Cell[54177, 2033, 127, 3, 55, "Input"],
Cell[54307, 2038, 224, 5, 55, "Input"],
Cell[54534, 2045, 187, 5, 55, "Input"],
Cell[54724, 2052, 272, 6, 55, "Input"],
Cell[54999, 2060, 228, 6, 55, "Input"],
Cell[55230, 2068, 273, 6, 55, "Input"],
Cell[55506, 2076, 220, 6, 55, "Input"],
Cell[55729, 2084, 436, 11, 94, "Input"],
Cell[56168, 2097, 383, 11, 55, "Input"],
Cell[56554, 2110, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[56647, 2117, 146, 5, 93, "Section"],
Cell[CellGroupData[{
Cell[56818, 2126, 144, 3, 68, "Subsection"],
Cell[56965, 2131, 361, 14, 93, "Text"],
Cell[57329, 2147, 85, 2, 55, "Input"],
Cell[57417, 2151, 744, 22, 98, "Input"],
Cell[58164, 2175, 84, 2, 55, "Input"],
Cell[58251, 2179, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[58332, 2185, 129, 3, 68, "Subsection"],
Cell[58464, 2190, 255, 6, 92, "Text"],
Cell[58722, 2198, 100, 2, 55, "Input"],
Cell[58825, 2202, 140, 4, 55, "Input"],
Cell[58968, 2208, 1980, 54, 360, "Input"],
Cell[60951, 2264, 99, 2, 55, "Input"],
Cell[61053, 2268, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[61134, 2274, 178, 6, 68, "Subsection"],
Cell[61315, 2282, 265, 6, 92, "Text"],
Cell[61583, 2290, 110, 3, 55, "Input"],
Cell[61696, 2295, 983, 28, 208, "Input"],
Cell[62682, 2325, 109, 3, 55, "Input"],
Cell[62794, 2330, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[62887, 2337, 160, 5, 93, "Section"],
Cell[CellGroupData[{
Cell[63072, 2346, 93, 2, 68, "Subsection"],
Cell[CellGroupData[{
Cell[63190, 2352, 52, 0, 58, "Subsubsection"],
Cell[63245, 2354, 344, 7, 60, "Text"],
Cell[63592, 2363, 90, 2, 55, "Input"],
Cell[63685, 2367, 44, 0, 59, "Text"],
Cell[63732, 2369, 239, 6, 55, "Input"],
Cell[63974, 2377, 90, 2, 59, "Text"],
Cell[64067, 2381, 304, 10, 55, "Input"],
Cell[64374, 2393, 169, 7, 60, "Text"],
Cell[64546, 2402, 118, 2, 55, "Input"],
Cell[64667, 2406, 274, 9, 93, "Text"],
Cell[64944, 2417, 420, 12, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[65401, 2434, 52, 0, 58, "Subsubsection"],
Cell[65456, 2436, 389, 10, 60, "Text"],
Cell[65848, 2448, 90, 2, 55, "Input"],
Cell[65941, 2452, 239, 6, 55, "Input"],
Cell[66183, 2460, 81, 2, 55, "Input"],
Cell[66267, 2464, 331, 11, 55, "Input"],
Cell[66601, 2477, 118, 2, 55, "Input"],
Cell[66722, 2481, 59, 1, 55, "Input"],
Cell[66784, 2484, 420, 12, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[67241, 2501, 52, 0, 58, "Subsubsection"],
Cell[67296, 2503, 285, 8, 60, "Text"],
Cell[67584, 2513, 90, 2, 55, "Input"],
Cell[67677, 2517, 173, 4, 55, "Input"],
Cell[67853, 2523, 81, 2, 55, "Input"],
Cell[67937, 2527, 201, 7, 55, "Input"],
Cell[68141, 2536, 118, 2, 55, "Input"],
Cell[68262, 2540, 59, 1, 55, "Input"],
Cell[68324, 2543, 420, 12, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[68781, 2560, 52, 0, 58, "Subsubsection"],
Cell[68836, 2562, 509, 16, 60, "Text"],
Cell[69348, 2580, 90, 2, 55, "Input"],
Cell[69441, 2584, 173, 4, 55, "Input"],
Cell[69617, 2590, 81, 2, 55, "Input"],
Cell[69701, 2594, 306, 10, 55, "Input"],
Cell[70010, 2606, 118, 2, 55, "Input"],
Cell[70131, 2610, 447, 13, 61, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[70615, 2628, 52, 0, 58, "Subsubsection"],
Cell[70670, 2630, 69, 1, 60, "Text"],
Cell[70742, 2633, 423, 9, 93, "Text"],
Cell[71168, 2644, 90, 2, 55, "Input"],
Cell[71261, 2648, 239, 6, 55, "Input"],
Cell[71503, 2656, 439, 14, 55, "Input"],
Cell[71945, 2672, 228, 8, 55, "Input"],
Cell[72176, 2682, 177, 4, 55, "Input"],
Cell[72356, 2688, 95, 2, 55, "Input"],
Cell[72454, 2692, 559, 16, 103, "Input"],
Cell[73016, 2710, 558, 16, 103, "Input"],
Cell[73577, 2728, 558, 16, 103, "Input"],
Cell[74138, 2746, 69, 1, 60, "Text"],
Cell[74210, 2749, 425, 9, 93, "Text"],
Cell[74638, 2760, 90, 2, 55, "Input"],
Cell[74731, 2764, 239, 6, 55, "Input"],
Cell[74973, 2772, 439, 14, 55, "Input"],
Cell[75415, 2788, 481, 16, 61, "Input"],
Cell[75899, 2806, 239, 7, 55, "Input"],
Cell[76141, 2815, 95, 2, 55, "Input"],
Cell[76239, 2819, 559, 16, 103, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[76835, 2840, 52, 0, 58, "Subsubsection"],
Cell[76890, 2842, 90, 2, 55, "Input"],
Cell[76983, 2846, 239, 6, 55, "Input"],
Cell[77225, 2854, 211, 7, 55, "Input"],
Cell[77439, 2863, 225, 7, 55, "Input"],
Cell[77667, 2872, 223, 7, 55, "Input"],
Cell[77893, 2881, 213, 5, 55, "Input"],
Cell[78109, 2888, 95, 2, 55, "Input"],
Cell[78207, 2892, 559, 16, 103, "Input"],
Cell[78769, 2910, 558, 16, 103, "Input"],
Cell[79330, 2928, 558, 16, 103, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[79925, 2949, 52, 0, 58, "Subsubsection"],
Cell[79980, 2951, 90, 2, 55, "Input"],
Cell[80073, 2955, 339, 8, 94, "Input"],
Cell[80415, 2965, 211, 7, 55, "Input"],
Cell[80629, 2974, 225, 7, 55, "Input"],
Cell[80857, 2983, 223, 7, 55, "Input"],
Cell[81083, 2992, 213, 5, 55, "Input"],
Cell[81299, 2999, 95, 2, 55, "Input"],
Cell[81397, 3003, 559, 16, 103, "Input"],
Cell[81959, 3021, 558, 16, 103, "Input"],
Cell[82520, 3039, 558, 16, 103, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[83115, 3060, 52, 0, 58, "Subsubsection"],
Cell[83170, 3062, 90, 2, 55, "Input"],
Cell[83263, 3066, 173, 4, 55, "Input"],
Cell[83439, 3072, 246, 8, 55, "Input"],
Cell[83688, 3082, 225, 7, 55, "Input"],
Cell[83916, 3091, 223, 7, 55, "Input"],
Cell[84142, 3100, 213, 5, 55, "Input"],
Cell[84358, 3107, 95, 2, 55, "Input"],
Cell[84456, 3111, 560, 16, 103, "Input"],
Cell[85019, 3129, 558, 16, 103, "Input"],
Cell[85580, 3147, 557, 16, 103, "Input"],
Cell[86140, 3165, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[86233, 3172, 105, 2, 68, "Subsection"],
Cell[CellGroupData[{
Cell[86363, 3178, 88, 1, 58, "Subsubsection"],
Cell[86454, 3181, 85, 2, 55, "Input"],
Cell[86542, 3185, 74, 2, 55, "Input"],
Cell[86619, 3189, 100, 3, 83, "Input"],
Cell[86722, 3194, 200, 5, 55, "Input"],
Cell[86925, 3201, 93, 2, 55, "Input"],
Cell[87021, 3205, 148, 4, 85, "Input"],
Cell[87172, 3211, 263, 8, 55, "Input"],
Cell[87438, 3221, 497, 14, 94, "Input"],
Cell[87938, 3237, 300, 9, 55, "Input"],
Cell[88241, 3248, 300, 9, 55, "Input"],
Cell[88544, 3259, 310, 9, 55, "Input"],
Cell[88857, 3270, 197, 5, 55, "Input"],
Cell[89057, 3277, 197, 5, 55, "Input"],
Cell[89257, 3284, 197, 5, 55, "Input"],
Cell[89457, 3291, 60, 1, 55, "Input"],
Cell[89520, 3294, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[89601, 3300, 102, 2, 58, "Subsubsection"],
Cell[89706, 3304, 85, 2, 55, "Input"],
Cell[89794, 3308, 74, 2, 55, "Input"],
Cell[89871, 3312, 100, 3, 83, "Input"],
Cell[89974, 3317, 200, 5, 55, "Input"],
Cell[90177, 3324, 93, 2, 55, "Input"],
Cell[90273, 3328, 148, 4, 85, "Input"],
Cell[90424, 3334, 517, 14, 94, "Input"],
Cell[90944, 3350, 310, 9, 55, "Input"],
Cell[91257, 3361, 310, 9, 55, "Input"],
Cell[91570, 3372, 310, 9, 55, "Input"],
Cell[91883, 3383, 197, 5, 55, "Input"],
Cell[92083, 3390, 197, 5, 55, "Input"],
Cell[92283, 3397, 197, 5, 55, "Input"],
Cell[92483, 3404, 60, 1, 55, "Input"],
Cell[92546, 3407, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[92627, 3413, 98, 2, 58, "Subsubsection"],
Cell[92728, 3417, 85, 2, 55, "Input"],
Cell[92816, 3421, 74, 2, 55, "Input"],
Cell[92893, 3425, 100, 3, 83, "Input"],
Cell[92996, 3430, 200, 5, 55, "Input"],
Cell[93199, 3437, 93, 2, 55, "Input"],
Cell[93295, 3441, 148, 4, 85, "Input"],
Cell[93446, 3447, 164, 4, 55, "Input"],
Cell[93613, 3453, 310, 9, 55, "Input"],
Cell[93926, 3464, 77, 2, 55, "Input"],
Cell[94006, 3468, 197, 5, 55, "Input"],
Cell[94206, 3475, 197, 5, 55, "Input"],
Cell[94406, 3482, 60, 1, 55, "Input"],
Cell[94469, 3485, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[94574, 3493, 133, 3, 92, "Section"],
Cell[CellGroupData[{
Cell[94732, 3500, 183, 5, 58, "Subsubsection"],
Cell[94918, 3507, 76, 2, 55, "Input"],
Cell[94997, 3511, 74, 2, 55, "Input"],
Cell[95074, 3515, 218, 7, 60, "Text"],
Cell[95295, 3524, 83, 2, 55, "Input"],
Cell[95381, 3528, 259, 8, 55, "Input"],
Cell[95643, 3538, 179, 7, 60, "Text"],
Cell[95825, 3547, 82, 2, 55, "Input"],
Cell[95910, 3551, 206, 7, 55, "Input"],
Cell[96119, 3560, 81, 2, 55, "Input"],
Cell[96203, 3564, 145, 6, 60, "Text"],
Cell[96351, 3572, 275, 9, 55, "Input"],
Cell[96629, 3583, 176, 4, 55, "Input"],
Cell[96808, 3589, 378, 17, 63, "Text"],
Cell[97189, 3608, 126, 3, 55, "Input"],
Cell[97318, 3613, 477, 15, 59, "Input"],
Cell[97798, 3630, 578, 18, 59, "Input"],
Cell[98379, 3650, 172, 5, 55, "Input"],
Cell[98554, 3657, 125, 3, 55, "Input"],
Cell[98682, 3662, 175, 8, 60, "Text"],
Cell[98860, 3672, 301, 10, 87, "Input"],
Cell[99164, 3684, 291, 9, 55, "Input"],
Cell[99458, 3695, 503, 14, 94, "Input"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[100010, 3715, 51, 0, 92, "Section"],
Cell[100064, 3717, 167, 5, 60, "Text"],
Cell[100234, 3724, 166, 5, 60, "Text"],
Cell[100403, 3731, 235, 6, 92, "Text"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature ouDT2OOWha#brB1fglWh1yRP *)
