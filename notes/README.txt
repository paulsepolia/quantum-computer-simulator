
1. Here I present the Mathematica code that I developed
   in order to develop a Quantum Computer Simulator.
   I have used both sparse matrix and dense matrix techniques.
   Although the involved, in the Mathematica developed code, 
   matrices are not always sparse, the direct products of them,
   usually is sparse and the code which makes use of
   the sparse matrix techniques of Mathematica is very fast and
   RAM efficient. The code which uses only dense techniques is
   safe, in RAM cost sense and will never explode the RAM.  

   #=========================#
   #  Pavlos G. Galiatsatos  #
   #  2013/08/15,            #
   #  Oxford, UK             #
   #=========================#
