(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 9.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1063,         20]
NotebookDataLength[    121325,       4330]
NotebookOptionsPosition[    100692,       3753]
NotebookOutlinePosition[    103861,       3829]
CellTagsIndexPosition[    103818,       3826]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"Quit", "[", "]"}]], "Input"],

Cell[CellGroupData[{

Cell["The QCS Sparse", "Title"],

Cell[CellGroupData[{

Cell["1 \[LongRightArrow] Introduction", "Section"],

Cell[CellGroupData[{

Cell["1 \[LongRightArrow] The Purpose", "Subsection"],

Cell[TextData[{
 "In this research text I present all the fundamentals quantum computer \
operators and I make use of the corresponding quantum computer algebra to \
develop a quantum computer simulator. The main functions are ",
 StyleBox["1. ",
  FontWeight->"Bold"],
 "the ",
 StyleBox["stateVectorFunction1[list___]\[Congruent]SVF1[list___],",
  FontWeight->"Bold"],
 " which takes as arguments an arbitrary sequence of total ",
 StyleBox["n",
  FontWeight->"Bold"],
 " up and/or down qubits and constructs the initial n-qubit,",
 StyleBox[" 2. ",
  FontWeight->"Bold"],
 "the ",
 StyleBox["GeneralArrayFlattenOuterNoRam[stepFun_, OpT_] ",
  FontWeight->"Bold"],
 "function which takes as argument the initial n-qubit and the operators that \
act up it and evaluates the rest n-qubits. ",
 StyleBox["3. ",
  FontWeight->"Bold"],
 "the",
 StyleBox[" plotQCS[svFun_, {stepIn_, stepFin_}, {pos1_, pos2_}, opts___] ",
  FontWeight->"Bold"],
 "which",
 StyleBox[" ",
  FontWeight->"Bold"],
 "visualizes the states of the quantum computer."
}], "Text"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "2 \[LongRightArrow] Chapter 1 \[LongRightArrow]",
 StyleBox[" ",
  FontSlant->"Italic"],
 "The State Vectors"
}], "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 "1 \[LongRightArrow] The",
 StyleBox[" ",
  FontSlant->"Italic"],
 "qubit[{theta_,phi_}] "
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["qubit[{theta_,phi_}] ",
  FontWeight->"Bold"],
 "function, which gives ",
 StyleBox["the most general formalism ",
  FontWeight->"Bold"],
 "of ",
 StyleBox["a",
  FontWeight->"Bold"],
 " ",
 StyleBox["single",
  FontWeight->"Bold"],
 " ",
 StyleBox["qubit",
  FontWeight->"Bold"],
 " as a function of the variables ",
 StyleBox["theta ",
  FontWeight->"Bold"],
 "and",
 StyleBox[" phi.",
  FontWeight->"Bold"],
 "  "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qubit", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", 
   RowBox[{"theta", ",", "phi"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"theta", ",", "phi"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"theta", ",", "phi"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"qubit", "[", 
    RowBox[{"{", 
     RowBox[{"theta_", ",", "phi_"}], "}"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"Cos", "[", 
       FractionBox["theta", "2"], "]"}], ",", "0"}], "}"}], "+", 
    RowBox[{
     RowBox[{"Exp", "[", 
      RowBox[{"I", "*", "phi"}], "]"}], "*", 
     RowBox[{"{", 
      RowBox[{"0", ",", 
       RowBox[{"Sin", "[", 
        FractionBox["theta", "2"], "]"}]}], "}"}]}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qubit", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qubit", "[", 
   RowBox[{"{", 
    RowBox[{"theta", ",", "phi"}], "}"}], "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general arbitrary qubit.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{
   RowBox[{"qubit", "[", 
    RowBox[{"{", 
     RowBox[{"theta", ",", "phi"}], "}"}], "]"}], "=", 
   TagBox[
    RowBox[{"(", "\[NoBreak]", 
     TagBox[GridBox[{
        {
         RowBox[{"Cos", "[", 
          FractionBox["theta", "2"], "]"}]},
        {
         RowBox[{
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{"\[ImaginaryI]", " ", "phi"}]], " ", 
          RowBox[{"Sin", "[", 
           FractionBox["theta", "2"], "]"}]}]}
       },
       GridBoxAlignment->{
        "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
          "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.5599999999999999]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}],
      Column], "\[NoBreak]", ")"}],
    Function[BoxForm`e$, 
     MatrixForm[BoxForm`e$]]]}],
  FontFamily->"Times New Roman"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] The generalStateVector[{theta_,phi_},dimension_]\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["generalStateVector[{theta_,phi_},dimension_] ",
  FontWeight->"Bold"],
 "function, which gives ",
 StyleBox["the most general formalism",
  FontWeight->"Bold"],
 " of ",
 StyleBox["a",
  FontWeight->"Bold"],
 " ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["dimension",
  FontWeight->"Bold"],
 " ",
 StyleBox["qubit",
  FontWeight->"Bold"],
 " as a function of the variables ",
 StyleBox["theta[i] ",
  FontWeight->"Bold"],
 "and",
 StyleBox[" phi[i].",
  FontWeight->"Bold"],
 "  "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "generalStateVector", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"generalStateVector", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"theta_", ",", "phi_"}], "}"}], ",", "dimension_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Flatten", "@", 
    RowBox[{"Outer", "[", 
     RowBox[{"Times", ",", 
      RowBox[{"Apply", "[", 
       RowBox[{"Sequence", ",", 
        RowBox[{"Table", "[", 
         RowBox[{
          RowBox[{"qubit", "[", 
           RowBox[{"{", 
            RowBox[{
             RowBox[{"theta", "[", "i", "]"}], ",", 
             RowBox[{"phi", "[", "i", "]"}]}], "}"}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"i", ",", "1", ",", "dimension"}], "}"}]}], "]"}]}], 
       "]"}]}], "]"}]}], "/;", 
   RowBox[{
    RowBox[{"dimension", "\[GreaterEqual]", "1"}], "&&", 
    RowBox[{"dimension", "\[Element]", "Integers"}]}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "generalStateVector", "]"}], ";"}]], "Input"],

Cell[TextData[{
 "Example 1. Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general arbitrary",
  FontWeight->"Bold"],
 " ",
 StyleBox["1",
  FontWeight->"Bold"],
 "-",
 StyleBox["dimension qubit.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"generalStateVector", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"theta", ",", "phi"}], "}"}], ",", "1"}], "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Example 2. Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general arbitrary",
  FontWeight->"Bold"],
 " ",
 StyleBox["2",
  FontWeight->"Bold"],
 "-",
 StyleBox["dimensions qubit.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"generalStateVector", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"theta", ",", "phi"}], "}"}], ",", "2"}], "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Example 3. Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general arbitrary",
  FontWeight->"Bold"],
 " ",
 StyleBox["3",
  FontWeight->"Bold"],
 "-",
 StyleBox["dimensions qubit.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"generalStateVector", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"theta", ",", "phi"}], "}"}], ",", "3"}], "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
3 \[LongRightArrow] Chapter 2 \[LongRightArrow] The Quantum Operators\
\>", "Section"],

Cell[CellGroupData[{

Cell["\<\
1 \[LongRightArrow] The generalQuantumOperator22[a,b,c,d] \[Congruent] \
gQO22[a,b,c,d] \
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["generalQuantumOperator22[a_,b_,c_,d_] \[Congruent]",
  FontWeight->"Bold"],
 " ",
 StyleBox["gQO22[a_,b_,c_,d_]",
  FontWeight->"Bold"],
 " function, which gives the ",
 StyleBox["most general formalism",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["2\[Times]2 Quantum Hermitian Operator ",
  FontWeight->"Bold"],
 "as a function of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Reals {a,b,c,d}.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "generalQuantumOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "gQO22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", 
   RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"generalQuantumOperator22", "[", 
    RowBox[{"a_", ",", "b_", ",", "c_", ",", "d_"}], "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", "opTmp", "}"}], ",", 
     RowBox[{
      RowBox[{
       RowBox[{"opTmp", "[", "1", "]"}], "=", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Exp", "[", 
            RowBox[{
             RowBox[{"-", "I"}], "*", 
             FractionBox["d", "2"]}], "]"}], ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", 
           RowBox[{"Exp", "[", 
            RowBox[{"I", "*", 
             FractionBox["d", "2"]}], "]"}]}], "}"}]}], "}"}]}], ";", 
      RowBox[{
       RowBox[{"opTmp", "[", "2", "]"}], "=", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Cos", "[", 
            FractionBox["c", "2"], "]"}], ",", 
           RowBox[{"-", 
            RowBox[{"Sin", "[", 
             FractionBox["c", "2"], "]"}]}]}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Sin", "[", 
            FractionBox["c", "2"], "]"}], ",", 
           RowBox[{"Cos", "[", 
            FractionBox["c", "2"], "]"}]}], "}"}]}], "}"}]}], ";", 
      RowBox[{
       RowBox[{"opTmp", "[", "3", "]"}], "=", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Exp", "[", 
            RowBox[{
             RowBox[{"-", "I"}], "*", 
             FractionBox["b", "2"]}], "]"}], ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", 
           RowBox[{"Exp", "[", 
            RowBox[{"I", "*", 
             FractionBox["b", "2"]}], "]"}]}], "}"}]}], "}"}]}], ";", 
      RowBox[{
       RowBox[{"Exp", "[", 
        RowBox[{"I", "*", "a"}], "]"}], "*", 
       RowBox[{"Dot", "[", 
        RowBox[{"Apply", "[", 
         RowBox[{"Sequence", ",", 
          RowBox[{"Table", "[", 
           RowBox[{
            RowBox[{"opTmp", "[", "i", "]"}], ",", 
            RowBox[{"{", 
             RowBox[{"i", ",", "3", ",", "1", ",", 
              RowBox[{"-", "1"}]}], "}"}]}], "]"}]}], "]"}], "]"}]}]}]}], 
    "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"gQO22", "[", 
    RowBox[{"a_", ",", "b_", ",", "c_", ",", "d_"}], "]"}], ":=", 
   RowBox[{"generalQuantumOperator22", "[", 
    RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "generalQuantumOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "gQO22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"FullSimplify", "[", 
   RowBox[{
    RowBox[{"gQO22", "[", 
     RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ",", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "}"}], "\[Element]", 
     "Reals"}]}], "]"}], "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["most general",
  FontWeight->"Bold"],
 " ",
 StyleBox["2\[Times]2 Quantum Hermitian Operator.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"gQO22", "[", 
    RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "=", 
   RowBox[{
    RowBox[{"generalQuantumOperator22", "[", 
     RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "=", 
    RowBox[{"(", "\[NoBreak]", GridBox[{
       {
        RowBox[{
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{
           FractionBox["1", "2"], " ", "\[ImaginaryI]", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"2", " ", "a"}], "-", "b", "-", "d"}], ")"}]}]], " ", 
         RowBox[{"Cos", "[", 
          FractionBox["c", "2"], "]"}]}], 
        RowBox[{
         RowBox[{"-", 
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{
            FractionBox["1", "2"], " ", "\[ImaginaryI]", " ", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{"2", " ", "a"}], "-", "b", "+", "d"}], ")"}]}]]}], " ", 
         RowBox[{"Sin", "[", 
          FractionBox["c", "2"], "]"}]}]},
       {
        RowBox[{
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{
           FractionBox["1", "2"], " ", "\[ImaginaryI]", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"2", " ", "a"}], "+", "b", "-", "d"}], ")"}]}]], " ", 
         RowBox[{"Sin", "[", 
          FractionBox["c", "2"], "]"}]}], 
        RowBox[{
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{
           FractionBox["1", "2"], " ", "\[ImaginaryI]", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"2", " ", "a"}], "+", "b", "+", "d"}], ")"}]}]], " ", 
         RowBox[{"Cos", "[", 
          FractionBox["c", "2"], "]"}]}]}
      },
      GridBoxAlignment->{
       "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
        "RowsIndexed" -> {}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.7]}, 
          Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}]}]}], ",", 
  " ", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "}"}], "\[Element]", 
   "Reals"}]}]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] The doNothingOperator22 \[Congruent] qI\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["doNothingOperator22 \[Congruent] qI.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "doNothingOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qI", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qI", "=", 
   RowBox[{"doNothingOperator22", "=", 
    RowBox[{"SparseArray", "@", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"gQO22", "[", 
        RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "/.", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"a", "\[Rule]", "0"}], ",", 
         RowBox[{"b", "\[Rule]", "0"}], ",", 
         RowBox[{"c", "\[Rule]", "0"}], ",", 
         RowBox[{"d", "\[Rule]", "0"}]}], "}"}]}], ")"}]}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "doNothingOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qI", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"qI", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"doNothingOperator22", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["2\[Times]2 doNothingOperator22.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{"qI", "=", 
  RowBox[{"doNothingOperator22", "=", 
   RowBox[{"(", "\[NoBreak]", GridBox[{
      {"1", "0"},
      {"0", "1"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.7]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", 
    ")"}]}]}]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "3 \[LongRightArrow] The phaseShiftOperator22[phi_] \[Congruent] qPh",
 StyleBox["[phi_]",
  FontWeight->"Bold"]
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["phaseShiftOperator22[phi_] \[Congruent] qPh[phi_]",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "phaseShiftOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qPh", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"qPh", "[", "phi_", "]"}], ":=", 
   RowBox[{"SparseArray", "@", 
    RowBox[{"FullSimplify", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"gQO22", "[", 
        RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "/.", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"a", "\[Rule]", 
          FractionBox[
           RowBox[{"b", "+", "d"}], "2"]}], ",", 
         RowBox[{"c", "\[Rule]", "0"}]}], "}"}]}], "/.", 
      RowBox[{"{", 
       RowBox[{"b", "\[Rule]", 
        RowBox[{"phi", "-", "d"}]}], "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"phaseShiftOperator22", "[", "phi_", "]"}], ":=", 
   RowBox[{"qPh", "[", "phi", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "phaseShiftOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qPh", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qPh", "[", "phi", "]"}], "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["2\[Times]2 phaseShiftOperator22[phi].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qPh", "[", "phi", "]"}], "=", 
  RowBox[{
   RowBox[{"phaseShiftOperator22", "[", "phi", "]"}], "=", 
   RowBox[{"(", "\[NoBreak]", GridBox[{
      {"1", "0"},
      {"0", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"\[ImaginaryI]", " ", "phi"}]]}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.7]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", 
    ")"}]}]}]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "4 \[LongRightArrow] The hadamardOperator22 \[Congruent] qH",
 StyleBox[" ",
  FontWeight->"Bold",
  FontSlant->"Italic"]
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["hadamardOperator22 \[Congruent] qH",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qH", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "hadamardOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qH", "=", 
   RowBox[{"hadamardOperator22", "=", 
    RowBox[{"SparseArray", "@", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"gQO22", "[", 
        RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], "/.", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"a", "\[Rule]", 
          FractionBox["Pi", "2"]}], ",", 
         RowBox[{"b", "\[Rule]", 
          RowBox[{"3", "*", "Pi"}]}], ",", 
         RowBox[{"c", "\[Rule]", 
          FractionBox[
           RowBox[{"3", "*", "Pi"}], "2"]}], ",", 
         RowBox[{"d", "\[Rule]", "0"}]}], "}"}]}], ")"}]}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qH", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "hadamardOperator22", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"qH", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"hadamardOperator22", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["hadamardOperator22.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{"qH", "=", 
   RowBox[{"hadamardOperator22", "=", 
    RowBox[{"(", "\[NoBreak]", GridBox[{
       {
        FractionBox["1", 
         SqrtBox["2"]], 
        FractionBox["1", 
         SqrtBox["2"]]},
       {
        FractionBox["1", 
         SqrtBox["2"]], 
        RowBox[{"-", 
         FractionBox["1", 
          SqrtBox["2"]]}]}
      },
      GridBoxAlignment->{
       "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
        "RowsIndexed" -> {}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.7]}, 
          Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}]}]}],
  FontFamily->"Times New Roman"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "5 \[LongRightArrow] The controlledNotOperator44 \[Congruent] ",
 StyleBox["qCN",
  FontWeight->"Bold"]
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["controlledNotOperator44 \[Congruent] qCN",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qCN", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "controlledNotOperator44", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qCN", "=", 
   RowBox[{"controlledNotOperator44", "=", 
    RowBox[{"SparseArray", "@", 
     RowBox[{"(", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"1", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "0", ",", "1"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "1", ",", "0"}], "}"}]}], "}"}], 
      ")"}]}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qCN", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "controlledNotOperator44", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"qCN", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"controlledNotOperator44", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["controlledNotOperator44.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{"qCN", "=", 
  RowBox[{"controlledNotOperator44", "=", 
   TagBox[
    StyleBox[
     RowBox[{"(", "\[NoBreak]", GridBox[{
        {"1", "0", "0", "0"},
        {"0", "1", "0", "0"},
        {"0", "0", "0", "1"},
        {"0", "0", "1", "0"}
       },
       GridBoxAlignment->{
        "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
          "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.7]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
     FontFamily->"Georgia"],
    Function[BoxForm`e$, 
     MatrixForm[BoxForm`e$]]]}]}]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
6 \[LongRightArrow] The controlledPhaseShiftOperator44[phi_] \[Congruent] \
qCPh[phi_]\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["controlledPhaseShiftOperator44[phi_] \[Congruent] qCPh[phi_].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qCPh", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "controlledPhaseShiftOperator44", "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "phi", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"qCPh", "[", "phi_", "]"}], ":=", 
   RowBox[{"SparseArray", "@", 
    RowBox[{"(", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "1", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0", ",", 
         RowBox[{"Exp", "[", 
          RowBox[{"I", "*", "phi"}], "]"}]}], "}"}]}], "}"}], ")"}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"controlledPhaseShiftOperator44", "[", "phi_", "]"}], ":=", 
   RowBox[{"qCPh", "[", "phi", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qCPh", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "controlledPhaseShiftOperator44", "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qCPh", "[", "phi", "]"}], "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"controlledPhaseShiftOperator44", "[", "phi", "]"}], "//", 
  "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["controlledPhaseShiftOperator44[phi].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{
   RowBox[{"qCPh", "[", "phi", "]"}], "=", 
   RowBox[{
    RowBox[{"controlledPhaseShiftOperator44", "[", "phi", "]"}], "=", 
    TagBox[
     StyleBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
         {"1", "0", "0", "0"},
         {"0", "1", "0", "0"},
         {"0", "0", "1", "0"},
         {"0", "0", "0", 
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{"\[ImaginaryI]", " ", "phi"}]]}
        },
        GridBoxAlignment->{
         "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.7]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
      FontFamily->"Georgia"],
     Function[BoxForm`e$, 
      MatrixForm[BoxForm`e$]]]}]}],
  FontFamily->"Times New Roman"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
7 \[LongRightArrow] The controlledControlledNotOperator88 \[Congruent] q2CN\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["controlledControlledNotOperator88 \[Congruent] q2CN",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "q2CN", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "controlledControlledNotOperator88", "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"q2CN", "=", 
   RowBox[{"controlledControlledNotOperator88", "=", 
    RowBox[{"SparseArray", "@", 
     RowBox[{"(", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{
         "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",",
           "1"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "1", ",",
           "0"}], "}"}]}], "}"}], ")"}]}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "q2CN", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "controlledControlledNotOperator88", "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"q2CN", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"controlledControlledNotOperator88", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["controlledControlledNotOperator88.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{"q2CN", "=", 
   RowBox[{"controlledControlledNotOperator88", "=", 
    TagBox[
     RowBox[{"(", "\[NoBreak]", GridBox[{
        {"1", "0", "0", "0", "0", "0", "0", "0"},
        {"0", "1", "0", "0", "0", "0", "0", "0"},
        {"0", "0", "1", "0", "0", "0", "0", "0"},
        {"0", "0", "0", "1", "0", "0", "0", "0"},
        {"0", "0", "0", "0", "1", "0", "0", "0"},
        {"0", "0", "0", "0", "0", "1", "0", "0"},
        {"0", "0", "0", "0", "0", "0", "0", "1"},
        {"0", "0", "0", "0", "0", "0", "1", "0"}
       },
       GridBoxAlignment->{
        "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
          "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.7]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
     Function[BoxForm`e$, 
      MatrixForm[BoxForm`e$]]]}]}],
  FontFamily->"Times New Roman"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["8 \[LongRightArrow] The fredkinOperator88 \[Congruent] qF", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["fredkinOperator88 \[Congruent] qF.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "qF", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "fredkinOperator88", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"qF", "=", 
   RowBox[{"fredkinOperator88", "=", 
    RowBox[{"SparseArray", "@", 
     RowBox[{"(", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{
         "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "1", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", "0", ",",
           "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
         "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0", ",",
           "1"}], "}"}]}], "}"}], ")"}]}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "qF", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "fredkinOperator88", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"qF", "//", "MatrixForm"}]], "Input"],

Cell[BoxData[
 RowBox[{"fredkinOperator88", "//", "MatrixForm"}]], "Input"],

Cell[TextData[{
 "Here is ",
 StyleBox["the Traditional Form",
  FontWeight->"Bold"],
 " of ",
 StyleBox["the",
  FontWeight->"Bold"],
 " ",
 StyleBox["fredkinOperator88.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 StyleBox[
  RowBox[{"qF", "=", 
   RowBox[{"fredkinOperator88", "=", 
    TagBox[
     RowBox[{"(", "\[NoBreak]", GridBox[{
        {"1", "0", "0", "0", "0", "0", "0", "0"},
        {"0", "1", "0", "0", "0", "0", "0", "0"},
        {"0", "0", "1", "0", "0", "0", "0", "0"},
        {"0", "0", "0", "1", "0", "0", "0", "0"},
        {"0", "0", "0", "0", "1", "0", "0", "0"},
        {"0", "0", "0", "0", "0", "0", "1", "0"},
        {"0", "0", "0", "0", "0", "1", "0", "0"},
        {"0", "0", "0", "0", "0", "0", "0", "1"}
       },
       GridBoxAlignment->{
        "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
          "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.7]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
     Function[BoxForm`e$, 
      MatrixForm[BoxForm`e$]]]}]}],
  FontFamily->"Georgia"]], "DisplayFormulaNumbered",
 FontFamily->"Times New Roman"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
4 \[LongRightArrow] Chapter 3 \[LongRightArrow] The Arbitrary State Vectors\
\>", "Section"],

Cell[CellGroupData[{

Cell["\<\
1 \[LongRightArrow] The up && down State Vectors of 1\[Dash]qubit\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["up",
  FontWeight->"Bold"],
 " and ",
 StyleBox["down",
  FontWeight->"Bold"],
 " qubits."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"up", ",", "down"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"up", "=", 
   RowBox[{"SparseArray", "@", 
    RowBox[{"{", 
     RowBox[{"1", ",", "0"}], "}"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"down", "=", 
   RowBox[{"SparseArray", "@", 
    RowBox[{"{", 
     RowBox[{"0", ",", "1"}], "}"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"up", ",", "down"}], "]"}], ";"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] The Base State Vectors of n\[Dash]Qubits\
\>", "Subsection"],

Cell[CellGroupData[{

Cell[TextData[{
 "1 \[LongRightArrow] ",
 StyleBox["Definition of",
  FontWeight->"Plain"],
 " stateVectorFunction1[list___] ",
 StyleBox["function.",
  FontWeight->"Plain"]
}], "Subsubsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["stateVectorFunction1[list___] ",
  FontWeight->"Bold"],
 "function which takes as arquments a sequence of ",
 StyleBox["up ",
  FontWeight->"Bold"],
 "and/or ",
 StyleBox["down qubits ",
  FontWeight->"Bold"],
 "and gives",
 StyleBox[" ",
  FontWeight->"Bold"],
 "as result the \"matrix\" product of them. "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"stateVectorFunction1", ",", "SVF1"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stateVectorFunction1", "[", "list___", "]"}], ":=", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"Block", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"aTmp1", ",", "aTmp2", ",", "aTmp3", ",", "aTmp4"}], "}"}], 
       ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"aTmp1", "=", 
         RowBox[{"Length", "[", 
          RowBox[{"{", "list", "}"}], "]"}]}], ";", "\[IndentingNewLine]", 
        RowBox[{"Do", "[", 
         RowBox[{
          RowBox[{
           RowBox[{"If", "[", 
            RowBox[{
             RowBox[{"i", "\[Equal]", "1"}], ",", 
             RowBox[{"aTmp2", "=", 
              RowBox[{"Part", "[", 
               RowBox[{
                RowBox[{"{", "list", "}"}], ",", "1"}], "]"}]}], ",", 
             RowBox[{"aTmp2", "=", "aTmp4"}]}], "]"}], ";", 
           "\[IndentingNewLine]", 
           RowBox[{"aTmp3", "=", 
            RowBox[{"Part", "[", 
             RowBox[{
              RowBox[{"{", "list", "}"}], ",", 
              RowBox[{"i", "+", "1"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
           RowBox[{"aTmp4", "=", 
            RowBox[{"Flatten", "[", 
             RowBox[{"Outer", "[", 
              RowBox[{"Times", ",", "aTmp2", ",", "aTmp3"}], "]"}], "]"}]}]}],
           ",", "\[IndentingNewLine]", 
          RowBox[{"{", 
           RowBox[{"i", ",", "1", ",", 
            RowBox[{"aTmp1", "-", "1"}]}], "}"}]}], "]"}], ";", 
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{"aTmp1", "\[Equal]", "1"}], ",", 
          RowBox[{"aTmp4", "=", "list"}]}], "]"}], ";", "aTmp4"}]}], "]"}], 
     ")"}], "/;", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"MemberQ", "[", 
       RowBox[{
        RowBox[{"{", "list", "}"}], ",", "up"}], "]"}], "||", 
      RowBox[{"MemberQ", "[", 
       RowBox[{
        RowBox[{"{", "list", "}"}], ",", "down"}], "]"}]}], ")"}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"SVF1", "[", "list___", "]"}], ":=", 
   RowBox[{"stateVectorFunction1", "[", "list", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "stateVectorFunction1", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "SVF1", "]"}], ";"}]], "Input"],

Cell["Some Examples are the following.", "Text"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", "down", "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"up", ",", "up", ",", "down"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"down", ",", "up", ",", "down", ",", "up"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"down", ",", "up", ",", "up", ",", "down", ",", "down"}], 
   "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"SVF1", "[", 
   RowBox[{"down", ",", "up", ",", "up", ",", "down", ",", "down"}], 
   "]"}]}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "2 \[LongRightArrow] ",
 StyleBox["Definition of",
  FontWeight->"Plain"],
 " upFunction[n_], downFunction[n] ",
 StyleBox["functions",
  FontWeight->"Plain"]
}], "Subsubsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["upFunction[n_] ",
  FontWeight->"Bold"],
 "and",
 StyleBox[" downFunction[n_] ",
  FontWeight->"Bold"],
 "functions which takes as arquments ",
 StyleBox["the integer valued variable",
  FontWeight->"Bold"],
 " ",
 StyleBox["n ",
  FontWeight->"Bold"],
 "and give back the \"matrix\" product of the ",
 StyleBox["n up ",
  FontWeight->"Bold"],
 "or",
 StyleBox[" down qubits.",
  FontWeight->"Bold"],
 " "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"upFunction", ",", "downFunction", ",", "upF", ",", "downF"}], 
   "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"upFunction", "[", "n_", "]"}], ":=", 
   RowBox[{"Apply", "[", 
    RowBox[{"Sequence", ",", 
     RowBox[{"Table", "[", 
      RowBox[{"up", ",", 
       RowBox[{"{", "n", "}"}]}], "]"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"downFunction", "[", "n_", "]"}], ":=", 
   RowBox[{"Apply", "[", 
    RowBox[{"Sequence", ",", 
     RowBox[{"Table", "[", 
      RowBox[{"down", ",", 
       RowBox[{"{", "n", "}"}]}], "]"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"upF", "[", "n_", "]"}], ":=", 
   RowBox[{"upFunction", "[", "n", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"downF", "[", "n_", "]"}], ":=", 
   RowBox[{"downFunction", "[", "n", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"upFunction", ",", "downFunction", ",", "upF", ",", "downF"}], 
   "]"}], ";"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["3 \[LongRightArrow] Some Examples", "Subsubsection"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"List", "@", 
   RowBox[{"downFunction", "[", "4", "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"List", "@", 
   RowBox[{"downF", "[", "4", "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"List", "@", 
   RowBox[{"upFunction", "[", "3", "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"List", "@", 
   RowBox[{"upF", "[", "3", "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"upF", "[", "10", "]"}], "===", 
  RowBox[{"upFunction", "[", "10", "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"stateVectorFunction1", "[", "down", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", "down", "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"up", ",", "up"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"down", ",", "up", ",", 
    RowBox[{"downFunction", "[", "2", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"downFunction", "[", "1", "]"}], ",", 
    RowBox[{"upFunction", "[", "2", "]"}], ",", 
    RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"SVF1", "[", 
   RowBox[{
    RowBox[{"downF", "[", "1", "]"}], ",", 
    RowBox[{"upF", "[", "2", "]"}], ",", 
    RowBox[{"downF", "[", "1", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"downFunction", "[", "11", "]"}], ",", 
    RowBox[{"upFunction", "[", "2", "]"}], ",", 
    RowBox[{"downFunction", "[", "1", "]"}]}], "]"}], "===", 
  RowBox[{"SVF1", "[", 
   RowBox[{
    RowBox[{"downF", "[", "11", "]"}], ",", 
    RowBox[{"upF", "[", "2", "]"}], ",", 
    RowBox[{"downF", "[", "1", "]"}]}], "]"}]}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
4 \[LongRightArrow] Definition of stateVectorFunction2[list___]\
\>", "Subsubsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["stateVectorFunction2[list___] ",
  FontWeight->"Bold"],
 "function which takes as arquments a sequence of ",
 StyleBox["up ",
  FontWeight->"Bold"],
 "and/or ",
 StyleBox["down qubits ",
  FontWeight->"Bold"],
 "and gives",
 StyleBox[" ",
  FontWeight->"Bold"],
 "as result the \"matrix\" product of them plus the ",
 StyleBox["dimension",
  FontWeight->"Bold"],
 " ",
 StyleBox["n ",
  FontWeight->"Bold"],
 "of the resulting ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubit,",
  FontWeight->"Bold"],
 " the ",
 StyleBox["array length ",
  FontWeight->"Bold"],
 "of the ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubit, ",
  FontWeight->"Bold"],
 "and finally the place location of the",
 StyleBox[" \"1\" ",
  FontWeight->"Bold"],
 "element in the array.",
 StyleBox[" ",
  FontWeight->"Bold"],
 "   "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"stateVectorFunction2", ",", "SVF2"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stateVectorFunction2", "[", "list___", "]"}], ":=", 
   RowBox[{
    RowBox[{"Block", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"aTmp1", ",", "aTmp2", ",", "aTmp3", ",", "aTmp4"}], "}"}], 
      ",", 
      RowBox[{
       RowBox[{"aTmp1", "=", 
        RowBox[{"{", 
         RowBox[{"Length", "[", 
          RowBox[{"{", 
           RowBox[{"Evaluate", "[", "list", "]"}], "}"}], "]"}], "}"}]}], ";", 
       RowBox[{"aTmp2", "=", 
        SuperscriptBox["2", "aTmp1"]}], ";", 
       RowBox[{"aTmp3", "=", 
        RowBox[{"Flatten", "[", 
         RowBox[{"Position", "[", 
          RowBox[{
           RowBox[{"aTmp4", "=", 
            RowBox[{"stateVectorFunction1", "[", "list", "]"}]}], ",", "1"}], 
          "]"}], "]"}]}], ";", 
       RowBox[{"{", 
        RowBox[{"aTmp1", ",", "aTmp2", ",", "aTmp3", ",", "aTmp4"}], 
        "}"}]}]}], "]"}], "/;", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"MemberQ", "[", 
       RowBox[{
        RowBox[{"{", "list", "}"}], ",", "up"}], "]"}], "||", 
      RowBox[{"MemberQ", "[", 
       RowBox[{
        RowBox[{"{", "list", "}"}], ",", "down"}], "]"}]}], ")"}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"SVF2", "[", "list___", "]"}], ":=", 
   RowBox[{"stateVectorFunction2", "[", "list", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "stateVectorFunction2", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "SVF2", "]"}], ";"}]], "Input"],

Cell["An example follows.", "Text"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction2", "[", 
   RowBox[{
    RowBox[{"upFunction", "[", "1", "]"}], ",", 
    RowBox[{"downFunction", "[", "2", "]"}], ",", 
    RowBox[{"upFunction", "[", "0", "]"}], ",", 
    RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"SVF2", "[", 
   RowBox[{
    RowBox[{"upF", "[", "1", "]"}], ",", 
    RowBox[{"downF", "[", "2", "]"}], ",", 
    RowBox[{"upF", "[", "0", "]"}], ",", 
    RowBox[{"downF", "[", "1", "]"}]}], "]"}]}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
5 \[LongRightArrow] Definition of stateVectorFunction3 [list___]\
\>", "Subsubsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["stateVectorFunction3[list___] ",
  FontWeight->"Bold"],
 "function which takes as arquments a sequence of ",
 StyleBox["up ",
  FontWeight->"Bold"],
 "and/or ",
 StyleBox["down qubits ",
  FontWeight->"Bold"],
 "and gives",
 StyleBox[" ",
  FontWeight->"Bold"],
 "as result the",
 StyleBox[" decomposed array",
  FontWeight->"Bold"],
 ", the elements of which are in sequence the qubits that composes the \
general ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubit",
  FontWeight->"Bold"],
 "."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"stateVectorFunction3", ",", "SVF3"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stateVectorFunction3", "[", "listSV___", "]"}], ":=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"aTmp1", ",", "aTmp2", ",", "aTmp3", ",", "aTmp4"}], "}"}], ",", 
     RowBox[{
      RowBox[{"aTmp1", "=", "listSV"}], ";", "\[IndentingNewLine]", 
      RowBox[{"aTmp4", "=", 
       RowBox[{"{", "}"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"aTmp2", "=", 
       RowBox[{"Length", "[", "aTmp1", "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"Do", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"aTmp2", "=", 
          RowBox[{"Length", "[", "aTmp1", "]"}]}], ";", "\[IndentingNewLine]", 
         RowBox[{"aTmp3", "=", 
          RowBox[{"Part", "[", 
           RowBox[{
            RowBox[{"Position", "[", 
             RowBox[{"aTmp1", ",", "1"}], "]"}], ",", "1", ",", "1"}], 
           "]"}]}], ";", "\[IndentingNewLine]", 
         RowBox[{"If", "[", 
          RowBox[{
           RowBox[{"aTmp3", "\[LessEqual]", 
            FractionBox["aTmp2", "2"]}], ",", 
           RowBox[{
            RowBox[{"aTmp4", "=", 
             RowBox[{"Append", "[", 
              RowBox[{"aTmp4", ",", "up"}], "]"}]}], ";", 
            RowBox[{"aTmp1", "=", 
             RowBox[{"Take", "[", 
              RowBox[{"aTmp1", ",", 
               FractionBox["aTmp2", "2"]}], "]"}]}]}], ",", 
           RowBox[{
            RowBox[{"aTmp4", "=", 
             RowBox[{"Append", "[", 
              RowBox[{"aTmp4", ",", "down"}], "]"}]}], ";", 
            RowBox[{"aTmp1", "=", 
             RowBox[{"Take", "[", 
              RowBox[{"aTmp1", ",", 
               RowBox[{"-", 
                FractionBox["aTmp2", "2"]}]}], "]"}]}]}]}], "]"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", 
          RowBox[{"Log", "[", 
           RowBox[{"2", ",", "aTmp2"}], "]"}]}], "}"}]}], "]"}], ";", 
      "\[IndentingNewLine]", "aTmp4"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"SVF3", "[", "listSV___", "]"}], ":=", 
   RowBox[{"stateVectorFunction3", "[", "listSV", "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"stateVectorFunction3", ",", "SVF3"}], "]"}], ";"}]], "Input"],

Cell["Some examples are the following.", "Text"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction3", "[", 
   RowBox[{"Normal", "@", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{
      RowBox[{"upFunction", "[", "2", "]"}], ",", 
      RowBox[{"downFunction", "[", "3", "]"}], ",", 
      RowBox[{"upFunction", "[", "1", "]"}]}], "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"SVF3", "[", 
   RowBox[{"Normal", "@", 
    RowBox[{"SVF1", "[", 
     RowBox[{
      RowBox[{"upF", "[", "2", "]"}], ",", 
      RowBox[{"downF", "[", "3", "]"}], ",", 
      RowBox[{"upF", "[", "1", "]"}]}], "]"}]}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"down", "\[Rule]", "\"\<down\>\""}], ",", 
    RowBox[{"up", "\[Rule]", "\"\<up\>\""}]}], "}"}]}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["3 \[LongRightArrow] Some Examples", "Subsection"],

Cell["\<\
Some examples how the previously defined functions works are the following.\
\>", "Text"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"List", "@", 
   RowBox[{"downFunction", "[", "7", "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"List", "@", 
   RowBox[{"downF", "[", "7", "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"List", "@", 
   RowBox[{"upFunction", "[", "10", "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"List", "@", 
   RowBox[{"upF", "[", "10", "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"down", ",", "down", ",", "down"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"SVF1", "[", 
   RowBox[{"down", ",", "down", ",", "down"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"up", ",", "up", ",", "down"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"SVF1", "[", 
   RowBox[{"up", ",", "up", ",", "down"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{"down", ",", "up", ",", 
    RowBox[{"downFunction", "[", "2", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"SVF1", "[", 
   RowBox[{"down", ",", "up", ",", 
    RowBox[{"downF", "[", "2", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction1", "[", 
   RowBox[{
    RowBox[{"downFunction", "[", "1", "]"}], ",", 
    RowBox[{"upFunction", "[", "2", "]"}], ",", 
    RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"SVF1", "[", 
   RowBox[{
    RowBox[{"downF", "[", "1", "]"}], ",", 
    RowBox[{"upF", "[", "2", "]"}], ",", 
    RowBox[{"downF", "[", "1", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction2", "[", 
   RowBox[{
    RowBox[{"upFunction", "[", "1", "]"}], ",", 
    RowBox[{"downFunction", "[", "2", "]"}], ",", 
    RowBox[{"upFunction", "[", "0", "]"}], ",", 
    RowBox[{"downFunction", "[", "0", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"SVF2", "[", 
   RowBox[{
    RowBox[{"upF", "[", "1", "]"}], ",", 
    RowBox[{"downF", "[", "2", "]"}], ",", 
    RowBox[{"upF", "[", "0", "]"}], ",", 
    RowBox[{"downF", "[", "0", "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction3", "[", 
   RowBox[{"Normal", "@", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{
      RowBox[{"upFunction", "[", "2", "]"}], ",", 
      RowBox[{"downFunction", "[", "3", "]"}], ",", 
      RowBox[{"upFunction", "[", "1", "]"}]}], "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"SVF3", "[", 
   RowBox[{"Normal", "@", 
    RowBox[{"SVF1", "[", 
     RowBox[{
      RowBox[{"upF", "[", "2", "]"}], ",", 
      RowBox[{"downF", "[", "3", "]"}], ",", 
      RowBox[{"upF", "[", "1", "]"}]}], "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"stateVectorFunction3", "[", 
   RowBox[{"Normal", "@", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{
      RowBox[{"upFunction", "[", "2", "]"}], ",", 
      RowBox[{"downFunction", "[", "3", "]"}], ",", 
      RowBox[{"upFunction", "[", "10", "]"}]}], "]"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"SVF3", "[", 
   RowBox[{"Normal", "@", 
    RowBox[{"SVF1", "[", 
     RowBox[{
      RowBox[{"upF", "[", "2", "]"}], ",", 
      RowBox[{"downF", "[", "3", "]"}], ",", 
      RowBox[{"upF", "[", "10", "]"}]}], "]"}]}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"down", "\[Rule]", "\"\<down\>\""}], ",", 
    RowBox[{"up", "\[Rule]", "\"\<up\>\""}]}], "}"}]}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "5 \[LongRightArrow] Chapter 4 \[LongRightArrow]",
 StyleBox[" ",
  FontSlant->"Italic"],
 "The Quantum Functions"
}], "Section"],

Cell[CellGroupData[{

Cell["\<\
1 \[LongRightArrow] The function \[LongRightArrow] plotQCS[svFun_, {stepIn_, \
stepFin_}, {pos1_, pos2_}, opts___]\
\>", "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["plotQCS[svFun_, {stepIn_, stepFin_}, {pos1_, pos2_}, opts___] ",
  FontWeight->"Bold"],
 "function which takes as argument ",
 StyleBox["m arbitrary",
  FontWeight->"Bold"],
 " ",
 StyleBox["n",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubits",
  FontWeight->"Bold"],
 " and plots the state of them."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "plotQCS", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"plotQCS", "[", 
    RowBox[{"svFun_", ",", 
     RowBox[{"{", 
      RowBox[{"stepIn_", ",", "stepFin_"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"pos1_", ",", "pos2_"}], "}"}], ",", "opts___"}], "]"}], ":=", 
   RowBox[{"ArrayPlot", "[", 
    RowBox[{
     RowBox[{"Transpose", "@", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"Take", "[", 
         RowBox[{
          SuperscriptBox[
           RowBox[{"svFun", "[", "i", "]"}], "2"], ",", 
          RowBox[{"{", 
           RowBox[{"pos1", ",", "pos2"}], "}"}]}], "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "stepIn", ",", "stepFin"}], "}"}]}], "]"}]}], ",", 
     "opts"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "plotQCS", "]"}], ";"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "2 \[LongRightArrow] The function \[LongRightArrow] GeneralSparseArrayNoRam[",
 StyleBox["stepFun_, OpT_",
  FontWeight->"Bold"],
 "]"
}], "Subsection"],

Cell[TextData[{
 "Here I define the ",
 StyleBox["GeneralSparseArrayNoRam[stepFun_, OpT_] ",
  FontWeight->"Bold"],
 "function. The purpose of this function is to build - having the quantum \
operators and the initial n-qubit - the rest n-qubits."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Unprotect", "[", 
   RowBox[{"KroneckerProduct", ",", "GeneralSparseArrayNoRam"}], "]"}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"KroneckerProduct", "[", "a_", "]"}], ":=", "a"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "KroneckerProduct", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "GeneralSparseArrayNoRam", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralSparseArrayNoRam", "[", 
    RowBox[{"stepFun_", ",", "OpT_"}], "]"}], ":=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", "aTmp1", "}"}], ",", 
     RowBox[{
      RowBox[{"aTmp1", "=", 
       RowBox[{"Length", "@", "OpT"}]}], ";", 
      RowBox[{"Do", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"stepFun", "[", 
          RowBox[{"i", "+", "1"}], "]"}], "=", 
         RowBox[{"SparseArray", "@", 
          RowBox[{"Dot", "[", 
           RowBox[{
            RowBox[{"Apply", "[", 
             RowBox[{"KroneckerProduct", ",", 
              RowBox[{"Part", "[", 
               RowBox[{"OpT", ",", 
                RowBox[{"aTmp1", "+", "1", "-", "i"}]}], "]"}]}], "]"}], ",", 
            RowBox[{"SparseArray", "@", 
             RowBox[{"stepFun", "[", "i", "]"}]}]}], "]"}]}]}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", 
          RowBox[{"Length", "@", "OpT"}]}], "}"}]}], "]"}], ";", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"stepFunction", "[", "i", "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", 
          RowBox[{"aTmp1", "+", "1"}]}], "}"}]}], "]"}]}]}], "]"}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "GeneralSparseArrayNoRam", "]"}], ";"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
6 \[LongRightArrow] Chapter 5 \[LongRightArrow] Some Examples Of Quantum \
Operations\
\>", "Section"],

Cell[CellGroupData[{

Cell["\<\
1 \[LongRightArrow] Some Typical Examples of Quantum Operations\
\>", "Subsection"],

Cell[CellGroupData[{

Cell["1 \[LongRightArrow] Example ", "Subsubsection"],

Cell[TextData[{
 "Here I evaluate the state vector ",
 StyleBox["\[LongRightArrow] ( \[CapitalEta] \[CircleTimes] \[CapitalIota] ) \
\[CircleTimes] ( CNOT ) \[CircleTimes] ( CNOT ) \[CircleTimes] ( \
\[CapitalEta] \[CircleTimes] \[CapitalIota] ) \[CircleTimes] \
\[VerticalSeparator] 1 0 \[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell["This is the initial 2-qubit.", "Text"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"stepFunction", "[", "1", "]"}], "=", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{
      RowBox[{"downFunction", "[", "1", "]"}], ",", 
      RowBox[{"upFunction", "[", "1", "]"}]}], "]"}]}], ")"}]}]], "Input"],

Cell["\<\
This is the sequence of operators that act upon the initial qubit.\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", "qCN", "}"}], ",", 
     RowBox[{"{", "qCN", "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[TextData[{
 "This is the array which holds as elements ",
 StyleBox["all the 2",
  FontWeight->"Bold"],
 "-",
 StyleBox["qubits.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"GeneralSparseArrayNoRam", "[", 
   RowBox[{"stepFunction", ",", "OpT"}], "]"}]}]], "Input"],

Cell[TextData[{
 "Here is the plot corresponding to the above Matrix - array of arrays. ",
 StyleBox["I denote with the",
  FontWeight->"Bold"],
 " ",
 StyleBox["Red color the exact zero's and with the Black color the exact \
one's.",
  FontWeight->"Bold"],
 " "
}], "Text"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Black"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["2 \[LongRightArrow] Example", "Subsubsection"],

Cell[TextData[{
 "Here I evaluate the state vector ",
 StyleBox["\[LongRightArrow] ",
  FontWeight->"Bold"],
 " ",
 StyleBox["( I \[CircleTimes] H ) \[CircleTimes] ( \[CapitalEta] \
\[CircleTimes] \[CapitalIota] ) \[CircleTimes] ( CNOT ) \[CircleTimes] ( \
\[CapitalEta] \[CircleTimes] H ) \[CircleTimes] \[VerticalSeparator] 0 1 \
\[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"stepFunction", "[", "1", "]"}], "=", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{
      RowBox[{"upFunction", "[", "1", "]"}], ",", 
      RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]}], ")"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "OpT", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qH"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", "qCN", "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qH"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"GeneralSparseArrayNoRam", "[", 
   RowBox[{"stepFunction", ",", "OpT"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "[", "%", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Black"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["3 \[LongRightArrow] Example", "Subsubsection"],

Cell[TextData[{
 "Here I evaluate the state vector ",
 StyleBox["\[LongRightArrow] ",
  FontWeight->"Bold"],
 " ",
 StyleBox[" ( CNOT ) \[CircleTimes] ( \[CapitalEta] \[CircleTimes] I ) \
\[CircleTimes] \[VerticalSeparator] 0 0 \[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"stepFunction", "[", "1", "]"}], "=", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{"upFunction", "[", "2", "]"}], "]"}]}], ")"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "OpT", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", "qCN", "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"GeneralSparseArrayNoRam", "[", 
   RowBox[{"stepFunction", ",", "OpT"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "[", "%", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Black"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["4 \[LongRightArrow] Example", "Subsubsection"],

Cell[TextData[{
 "Here I evaluate the state vector ",
 StyleBox["\[LongRightArrow] ",
  FontWeight->"Bold"],
 " ",
 StyleBox[" ( I \[CircleTimes] CNOT ) \[CircleTimes] ( CNOT ",
  FontWeight->"Bold"],
 Cell[BoxData[
  FormBox[
   StyleBox["\[CirclePlus]",
    FontWeight->"Bold"], TraditionalForm]],
  FontWeight->"Bold"],
 StyleBox[" I ) \[CircleTimes] ( \[CapitalEta] \[CircleTimes] I \
\[CircleTimes] I ) \[CircleTimes] \[VerticalSeparator] 0 0 0 \
\[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"stepFunction", "[", "1", "]"}], "=", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{"upFunction", "[", "3", "]"}], "]"}]}], ")"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "OpT", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qCN", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI", ",", "qI"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"GeneralSparseArrayNoRam", "[", 
   RowBox[{"stepFunction", ",", "OpT"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Black"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["5 \[LongRightArrow] Example", "Subsubsection"],

Cell[TextData[StyleBox["Variation 1.",
 FontWeight->"Bold"]], "Text"],

Cell[TextData[{
 "Here I act with the operator ",
 StyleBox["( I \[CircleTimes] CNOT ) \[CircleTimes] ( CNOT \[CircleTimes] I ) \
\[CircleTimes] ( \[CapitalEta] \[CircleTimes] I \[CircleTimes] I ) ",
  FontWeight->"Bold"],
 "several times and I observe the evolution of the quantum state vector. The \
initial vector is the  ",
 StyleBox["\[VerticalSeparator] 0 1 1 \[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"stepFunction", "[", "1", "]"}], "=", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{
      RowBox[{"upFunction", "[", "1", "]"}], ",", 
      RowBox[{"downFunction", "[", "2", "]"}]}], "]"}]}], ")"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qCN", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT2", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{"OpT1", ",", 
       RowBox[{"{", "200", "}"}]}], "]"}], ",", "1"}], "]"}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"StepsSparseNoRam", "=", 
     RowBox[{"GeneralSparseArrayNoRam", "[", 
      RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}], ")"}], "//", 
  "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "[", "StepsSparseNoRam", "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "1"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"20", ",", "240"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "5"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"40", ",", "140"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "12"}], "}"}]}]}], "]"}]], "Input"],

Cell[TextData[StyleBox["Variation 2.",
 FontWeight->"Bold"]], "Text"],

Cell[TextData[{
 "Here I act with each of the operators ",
 StyleBox["{ ( I \[CircleTimes] CNOT ) , ( CNOT \[CircleTimes] I ) , ( \
\[CapitalEta] \[CircleTimes] I \[CircleTimes] I ) } ",
  FontWeight->"Bold"],
 "in random order and I observe the evolution of the quantum state vector. \
The initial quantum state vector is the ",
 StyleBox["\[VerticalSeparator] 0 1 1 \[RightAngleBracket].",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"stepFunction", "[", "1", "]"}], "=", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{
      RowBox[{"upFunction", "[", "1", "]"}], ",", 
      RowBox[{"downFunction", "[", "2", "]"}]}], "]"}]}], ")"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qCN", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qH", ",", "qI", ",", "qI"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN"}], "}"}]}], "}"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"OpT2", "=", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"Part", "[", 
        RowBox[{"OpT1", ",", 
         RowBox[{"RandomInteger", "[", 
          RowBox[{"{", 
           RowBox[{"1", ",", 
            RowBox[{"Length", "[", "OpT1", "]"}]}], "}"}], "]"}]}], "]"}], 
       ",", 
       RowBox[{"{", 
        SuperscriptBox["10", "3"], "}"}]}], "]"}]}], ";"}], ")"}], "//", 
  "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"StepsSparseNoRam", "=", 
     RowBox[{"GeneralSparseArrayNoRam", "[", 
      RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}], ")"}], "//", 
  "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "[", "StepsSparseNoRam", "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "3"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"40", ",", "1"}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["6 \[LongRightArrow] Example", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"stepFunction", "[", "1", "]"}], "=", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{
      RowBox[{"upFunction", "[", "4", "]"}], ",", 
      RowBox[{"downFunction", "[", "0", "]"}]}], "]"}]}], ")"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"RR", "[", "n_", "]"}], ":=", 
  RowBox[{"RotateRight", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"qI", ",", "qCN", ",", "qH"}], "}"}], ",", "n"}], 
   "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"RR", "[", "i", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "1", ",", "400"}], "}"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT2", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{"OpT1", ",", 
       RowBox[{"{", "1", "}"}]}], "]"}], ",", "1"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"StepsSparseNoRam", "=", 
     RowBox[{"GeneralSparseArrayNoRam", "[", 
      RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}], ")"}], "//", 
  "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "[", "StepsSparseNoRam", "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "3"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"20", ",", "240"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "5"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"40", ",", "140"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "12"}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["7 \[LongRightArrow] Example", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"stepFunction", "[", "1", "]"}], "=", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{
      RowBox[{"upFunction", "[", "1", "]"}], ",", 
      RowBox[{"downFunction", "[", "1", "]"}], ",", 
      RowBox[{"upFunction", "[", "1", "]"}], ",", 
      RowBox[{"downFunction", "[", "1", "]"}]}], "]"}]}], ")"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"RR", "[", "n_", "]"}], ":=", 
  RowBox[{"RotateRight", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"qI", ",", "qCN", ",", "qH"}], "}"}], ",", "n"}], 
   "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"RR", "[", "i", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "1", ",", "400"}], "}"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT2", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{"OpT1", ",", 
       RowBox[{"{", "1", "}"}]}], "]"}], ",", "1"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"StepsSparseNoRam", "=", 
     RowBox[{"GeneralSparseArrayNoRam", "[", 
      RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}], ")"}], "//", 
  "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "@", "StepsSparseNoRam"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "3"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"20", ",", "240"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "5"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"40", ",", "140"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "4"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"30", ",", "12"}], "}"}]}]}], "]"}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["8 \[LongRightArrow] Example", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepFunction", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Normal", "@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"stepFunction", "[", "1", "]"}], "=", 
    RowBox[{"stateVectorFunction1", "[", 
     RowBox[{"upFunction", "[", "5", "]"}], "]"}]}], ")"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"RR", "[", "n_", "]"}], ":=", 
   RowBox[{"RotateRight", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"qI", ",", "qCN", ",", "qH", ",", "qH"}], "}"}], ",", "n"}], 
    "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT1", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"RR", "[", "i", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "1", ",", "100"}], "}"}]}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OpT2", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{"OpT1", ",", 
       RowBox[{"{", "1", "}"}]}], "]"}], ",", "1"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"StepsSparseNoRam", "=", 
     RowBox[{"GeneralSparseArrayNoRam", "[", 
      RowBox[{"stepFunction", ",", "OpT2"}], "]"}]}], ";"}], ")"}], "//", 
  "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"aTmp2", "=", 
  RowBox[{"Length", "@", "StepsSparseNoRam"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "aTmp2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "5"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"0", "\[Rule]", "Red"}], ",", 
      RowBox[{"1", "\[Rule]", "Green"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"10", ",", "10"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"73", ",", "98"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "5"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "False"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"10", ",", "10"}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepFunction", ",", 
   RowBox[{"{", 
    RowBox[{"48", ",", "98"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     SuperscriptBox["2", "5"]}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1", "\[Rule]", "Green"}], ",", 
      RowBox[{"0", "\[Rule]", "Red"}]}], "}"}]}], ",", 
   RowBox[{"PixelConstrained", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"10", ",", "10"}], "}"}]}]}], "]"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] Testing the Efficiency of the Sparse Matrices Techniques\
\>", "Subsection"],

Cell[CellGroupData[{

Cell["1 \[LongRightArrow] Example \[LongRightArrow] Sparse Matrices", \
"Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "@", "stepFunction"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numBits", "=", "12"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numOne", "=", 
   FractionBox["numBits", "3"]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepFunction", "[", "1", "]"}], "=", 
   RowBox[{"stateVectorFunction1", "[", 
    RowBox[{"upFunction", "[", "numBits", "]"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "@", 
  RowBox[{"stepFunction", "[", "1", "]"}]}]], "Input"],

Cell[BoxData[
 FractionBox[
  RowBox[{"ByteCount", "@", 
   RowBox[{"stepFunction", "[", "1", "]"}]}], 
  SuperscriptBox["1024.00", "2"]]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", "qI"}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", "qCN"}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", "qH"}]}]}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qI", ",", "qCN"}], "]"}]}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qCN", ",", "qI"}], "]"}]}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qH", ",", "qI", ",", "qI"}], "]"}]}]}]}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada1", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qI", ",", "qCN"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada2", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qCN", ",", "qI"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada3", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qH", ",", "qI", ",", "qI"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralSparseArrayNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada1", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralSparseArrayNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada2", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralSparseArrayNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada3", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
2 \[LongRightArrow] Example \[LongRightArrow] Sparse & Dense Matrices\
\>", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "@", "stepFunction"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numBits", "=", "12"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numOne", "=", 
   FractionBox["numBits", "3"]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepFunction", "[", "1", "]"}], "=", 
   RowBox[{"stateVectorFunction1", "[", 
    RowBox[{"upFunction", "[", "numBits", "]"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "@", 
  RowBox[{"stepFunction", "[", "1", "]"}]}]], "Input"],

Cell[BoxData[
 FractionBox[
  RowBox[{"ByteCount", "@", 
   RowBox[{"stepFunction", "[", "1", "]"}]}], 
  SuperscriptBox["1024.00", "2"]]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qI", ",", "qI", ",", "qH"}], "]"}]}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qI", ",", "qH", ",", "qH"}], "]"}]}]}], ",", 
   RowBox[{"ArrayPlot", "@", 
    RowBox[{"Abs", "@", 
     RowBox[{"KroneckerProduct", "[", 
      RowBox[{"qH", ",", "qH", ",", "qH"}], "]"}]}]}]}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada1", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qI", ",", "qI", ",", "qH"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada2", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qI", ",", "qH", ",", "qH"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada3", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qH", ",", "qH", ",", "qH"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralSparseArrayNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada1", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralSparseArrayNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada2", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralSparseArrayNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada3", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"NotebookSave", "[", "]"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
3 \[LongRightArrow] Example \[LongRightArrow] Only Dense Matrices\
\>", "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "@", "stepFunction"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numBits", "=", "12"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"numOne", "=", 
   FractionBox["numBits", "3"]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepFunction", "[", "1", "]"}], "=", 
   RowBox[{"stateVectorFunction1", "[", 
    RowBox[{"upFunction", "[", "numBits", "]"}], "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Length", "@", 
  RowBox[{"stepFunction", "[", "1", "]"}]}]], "Input"],

Cell[BoxData[
 FractionBox[
  RowBox[{"ByteCount", "@", 
   RowBox[{"stepFunction", "[", "1", "]"}]}], 
  SuperscriptBox["1024.00", "2"]]], "Input"],

Cell[BoxData[
 RowBox[{"ArrayPlot", "@", 
  RowBox[{"Abs", "@", 
   RowBox[{"KroneckerProduct", "[", 
    RowBox[{"qH", ",", "qH", ",", "qH"}], "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada1", "=", 
   RowBox[{"ReleaseHold", "@", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Hold", "@", 
       RowBox[{"Sequence", "[", 
        RowBox[{"qH", ",", "qH", ",", "qH"}], "]"}]}], ",", 
      RowBox[{"{", "numOne", "}"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"omada2", "=", "omada1"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralSparseArrayNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada1", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"GeneralSparseArrayNoRam", "[", 
    RowBox[{"stepFunction", ",", 
     RowBox[{"{", "omada2", "}"}]}], "]"}], ";"}], "//", "Timing"}]], "Input"],

Cell[BoxData[
 RowBox[{"NotebookSave", "[", "]"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "6 \[LongRightArrow] Chapter 6. ",
 StyleBox["Grover's Algorithm",
  FontSlant->"Italic"]
}], "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 "Here I make a graphical demonstration of the ",
 StyleBox["Grover's Algorithm",
  FontWeight->"Bold",
  FontSlant->"Italic"],
 StyleBox[". ",
  FontWeight->"Bold"]
}], "Subsubsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"DownQubits", "=", "0"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"UpQubits", "=", "8"}], ";"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 1. ",
  FontWeight->"Bold"],
 "Here I define the ",
 StyleBox["stepF[1]",
  FontWeight->"Bold"],
 " initial state of the quantum register. I made this in an arbitrary way.  "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "stepF", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepF", "[", "1", "]"}], "=", 
   RowBox[{"N", "@", 
    RowBox[{"SVF1", "[", 
     RowBox[{
      RowBox[{"downF", "@", "DownQubits"}], ",", 
      RowBox[{"upF", "@", "UpQubits"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 2. ",
  FontWeight->"Bold"],
 "Here I define the ",
 StyleBox["eigenvector ",
  FontWeight->"Bold"],
 "that I want finally to produce."
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", "WILF", "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"WILF", "=", 
   RowBox[{"N", "@", 
    RowBox[{"SVF1", "[", 
     RowBox[{"upF", "[", 
      RowBox[{"UpQubits", "+", "DownQubits"}], "]"}], "]"}]}]}], 
  ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", "WILF", "]"}], ";"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 3. ",
  FontWeight->"Bold"],
 "Here I make the ",
 StyleBox["first two steps.",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OperatorsTable", "=", 
   RowBox[{"N", "[", 
    RowBox[{"{", 
     RowBox[{"Table", "[", 
      RowBox[{"qH", ",", 
       RowBox[{"{", 
        RowBox[{"DownQubits", "+", "UpQubits"}], "}"}]}], "]"}], "}"}], 
    "]"}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"GeneralSparseArrayNoRam", "[", 
     RowBox[{"stepF", ",", "OperatorsTable"}], "]"}], ";"}], ")"}], "//", 
  "Timing"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 4. ",
  FontWeight->"Bold"],
 "Here I define the ",
 Cell[BoxData[
  FormBox[
   OverscriptBox["O", "^"], TraditionalForm]],
  FontWeight->"Bold",
  FontSlant->"Italic"],
 " and the ",
 Cell[BoxData[
  FormBox[
   StyleBox[
    OverscriptBox["S", "^"],
    FontWeight->"Bold"], TraditionalForm]]],
 StyleBox[".",
  FontWeight->"Bold"]
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ClearAll", "[", 
   RowBox[{"OOp", ",", "GOp", ",", "bothGrover"}], "]"}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"OOp", "=", 
    RowBox[{"N", "[", 
     RowBox[{
      RowBox[{"IdentityMatrix", "[", 
       SuperscriptBox["2", 
        RowBox[{"DownQubits", "+", "UpQubits"}]], "]"}], "-", 
      RowBox[{"2", 
       RowBox[{"Dot", "[", 
        RowBox[{
         RowBox[{"Transpose", "[", 
          RowBox[{"{", "WILF", "}"}], "]"}], ",", 
         RowBox[{"{", "WILF", "}"}]}], "]"}]}]}], "]"}]}], ";"}], 
  "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"GOp", "=", 
    RowBox[{"N", "[", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{"IdentityMatrix", "[", 
        SuperscriptBox["2", 
         RowBox[{"DownQubits", "+", "UpQubits"}]], "]"}]}], "+", 
      RowBox[{"2", 
       RowBox[{"Dot", "[", 
        RowBox[{
         RowBox[{"Transpose", "[", 
          RowBox[{"{", 
           RowBox[{"stepF", "[", "2", "]"}], "}"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"stepF", "[", "2", "]"}], "}"}]}], "]"}]}]}], "]"}]}], 
   ";"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{
   RowBox[{"bothGrover", "=", 
    RowBox[{"Dot", "[", 
     RowBox[{"GOp", ",", "OOp"}], "]"}]}], ";"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Protect", "[", 
   RowBox[{"OOp", ",", "GOp", ",", "bothGrover"}], "]"}], ";"}]], "Input"],

Cell[TextData[{
 StyleBox["Step 5. Now",
  FontWeight->"Bold"],
 " the ",
 StyleBox["Grover's Algorithm",
  FontWeight->"Bold",
  FontSlant->"Italic"],
 " starts."
}], "Text"],

Cell[BoxData[
 RowBox[{"periodConstant", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      FractionBox["Pi", "4"], "*", 
      SqrtBox[
       SuperscriptBox["2", 
        RowBox[{"UpQubits", "+", "DownQubits"}]]]}], "-", 
     FractionBox["1", "2"]}], ")"}], "//", "N"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"stepF", "[", "n_", "]"}], ":=", 
   RowBox[{
    RowBox[{"stepF", "[", "n", "]"}], "=", 
    RowBox[{"Dot", "[", 
     RowBox[{"bothGrover", ",", 
      RowBox[{"stepF", "[", 
       RowBox[{"n", "-", "1"}], "]"}]}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{"plotQCS", "[", 
  RowBox[{"stepF", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", 
     RowBox[{"2", "*", "2", "*", 
      RowBox[{"Ceiling", "[", "periodConstant", "]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "10"}], "}"}], ",", 
   RowBox[{"Mesh", "\[Rule]", "True"}], ",", 
   RowBox[{"ColorRules", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"1.", "\[Rule]", "Green"}], ",", 
      RowBox[{"0.", "\[Rule]", "Red"}]}], "}"}]}]}], "]"}]], "Input"],

Cell["", "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["7 \[LongRightArrow] Bibliography", "Section"],

Cell[TextData[{
 StyleBox["1.",
  FontWeight->"Bold"],
 " Quantum Computer Science: An Introduction. N. David Mermin. Cambridge \
University Press (2007)."
}], "Text"],

Cell[TextData[{
 StyleBox["2.",
  FontWeight->"Bold"],
 " Mathematics of Quantum Computation. Ranee K. Brylinski, Goong Chen. ",
 "Chapman & Hall (2002)."
}], "Text"],

Cell[TextData[{
 StyleBox["3.",
  FontWeight->"Bold"],
 " Principles of Quantum Computation and Information: Basic Concepts Vol 1. \
Giuliano Benenti, Giulio Casati, Giuliano Strini.\nWorld Scientific \
Publishing, (2004)."
}], "Text"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1596, 801},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
ShowCellBracket->False,
ShowSelection->True,
Deployed->True,
CellContext->Notebook,
TrackCellChangeTimes->False,
Magnification->1.,
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> 
     FrontEnd`FileName[{"Creative"}, "NaturalColor.nb", CharacterEncoding -> 
       "WindowsGreek"]]], 
   Cell[
    StyleData["Title"], FontFamily -> "Times New Roman", FontSize -> 36, 
    FontWeight -> "Bold", FontSlant -> "Plain", 
    FontVariations -> {"StrikeThrough" -> False, "Underline" -> False}, 
    FontColor -> GrayLevel[0], Background -> RGBColor[1, 0, 0]], 
   Cell[
    StyleData["Section"], FontFamily -> "Times New Roman", FontSize -> 34, 
    FontWeight -> "Bold", FontSlant -> "Plain", 
    FontVariations -> {"StrikeThrough" -> False, "Underline" -> False}, 
    FontColor -> GrayLevel[0], Background -> 
    RGBColor[0.49019607843137253`, 0.8, 0.8666666666666667]], 
   Cell[
    StyleData["Subsection"], CellDingbat -> None, FontFamily -> 
    "Times New Roman", FontSize -> 28, FontWeight -> "Bold", FontSlant -> 
    "Plain", FontVariations -> {
     "StrikeThrough" -> False, "Underline" -> False}, FontColor -> 
    GrayLevel[0], Background -> 
    RGBColor[0.6823529411764706, 0.8235294117647058, 0.41568627450980394`]], 
   Cell[
    StyleData["Input"], FontFamily -> "Courier New", FontSize -> 26, 
    Background -> 
    RGBColor[0.9882352941176471, 0.6784313725490196, 0.2196078431372549]], 
   Cell[
    StyleData["Output"], FontFamily -> "Courier New", FontSize -> 26, 
    FontColor -> GrayLevel[0], Background -> GrayLevel[1]], 
   Cell[
    StyleData["Text"], TextJustification -> 1., FontFamily -> 
    "Times New Roman", FontSize -> 26, FontWeight -> "Plain", FontSlant -> 
    "Plain", 
    FontVariations -> {"StrikeThrough" -> False, "Underline" -> False}, 
    FontColor -> GrayLevel[0], Background -> 
    RGBColor[0.9607843137254902, 0.8196078431372549, 0.996078431372549]], 
   Cell[
    StyleData["DisplayFormulaNumbered"], FontFamily -> "Times New Roman", 
    FontSize -> 26, FontWeight -> "Bold", FontSlant -> "Plain", 
    FontVariations -> {"StrikeThrough" -> False, "Underline" -> False}, 
    FontColor -> GrayLevel[0], Background -> 
    RGBColor[0.796078431372549, 0.803921568627451, 0.996078431372549]], 
   Cell[
    StyleData["Subsubsection"], CellDingbat -> None, FontFamily -> 
    "Times New Roman", FontSize -> 26, FontWeight -> "Bold", FontSlant -> 
    "Plain", FontVariations -> {
     "StrikeThrough" -> False, "Underline" -> False}, FontColor -> 
    GrayLevel[0], Background -> RGBColor[1, 0.5, 0.5]]}, 
  WindowSize -> {708, 681}, WindowMargins -> {{0, Automatic}, {Automatic, 0}},
   FrontEndVersion -> "9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
   StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[1463, 33, 52, 1, 55, "Input"],
Cell[CellGroupData[{
Cell[1540, 38, 31, 0, 87, "Title"],
Cell[CellGroupData[{
Cell[1596, 42, 51, 0, 92, "Section"],
Cell[CellGroupData[{
Cell[1672, 46, 53, 0, 68, "Subsection"],
Cell[1728, 48, 1050, 29, 223, "Text"],
Cell[2781, 79, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[2874, 86, 142, 5, 93, "Section"],
Cell[CellGroupData[{
Cell[3041, 95, 125, 5, 68, "Subsection"],
Cell[3169, 102, 478, 23, 93, "Text"],
Cell[3650, 127, 83, 2, 55, "Input"],
Cell[3736, 131, 110, 3, 55, "Input"],
Cell[3849, 136, 109, 3, 55, "Input"],
Cell[3961, 141, 108, 3, 55, "Input"],
Cell[4072, 146, 505, 17, 83, "Input"],
Cell[4580, 165, 82, 2, 55, "Input"],
Cell[4665, 169, 149, 5, 55, "Input"],
Cell[4817, 176, 259, 13, 60, "Text"],
Cell[5079, 191, 1133, 33, 112, "DisplayFormulaNumbered"],
Cell[6215, 226, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[6296, 232, 98, 2, 68, "Subsection"],
Cell[6397, 236, 555, 26, 93, "Text"],
Cell[6955, 264, 96, 2, 55, "Input"],
Cell[7054, 268, 843, 24, 170, "Input"],
Cell[7900, 294, 95, 2, 55, "Input"],
Cell[7998, 298, 366, 19, 60, "Text"],
Cell[8367, 319, 188, 6, 55, "Input"],
Cell[8558, 327, 367, 19, 60, "Text"],
Cell[8928, 348, 188, 6, 55, "Input"],
Cell[9119, 356, 367, 19, 60, "Text"],
Cell[9489, 377, 188, 6, 55, "Input"],
Cell[9680, 385, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9773, 392, 96, 2, 92, "Section"],
Cell[CellGroupData[{
Cell[9894, 398, 118, 3, 68, "Subsection"],
Cell[10015, 403, 539, 22, 93, "Text"],
Cell[10557, 427, 102, 2, 55, "Input"],
Cell[10662, 431, 83, 2, 55, "Input"],
Cell[10748, 435, 124, 3, 55, "Input"],
Cell[10875, 440, 123, 3, 55, "Input"],
Cell[11001, 445, 122, 3, 55, "Input"],
Cell[11126, 450, 2253, 68, 310, "Input"],
Cell[13382, 520, 247, 6, 55, "Input"],
Cell[13632, 528, 101, 2, 55, "Input"],
Cell[13736, 532, 82, 2, 55, "Input"],
Cell[13821, 536, 319, 9, 55, "Input"],
Cell[14143, 547, 323, 16, 60, "Text"],
Cell[14469, 565, 2319, 64, 122, "DisplayFormulaNumbered"],
Cell[16791, 631, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[16872, 637, 89, 2, 68, "Subsection"],
Cell[16964, 641, 122, 4, 60, "Text"],
Cell[17089, 647, 97, 2, 55, "Input"],
Cell[17189, 651, 80, 2, 55, "Input"],
Cell[17272, 655, 509, 15, 55, "Input"],
Cell[17784, 672, 96, 2, 55, "Input"],
Cell[17883, 676, 79, 2, 55, "Input"],
Cell[17965, 680, 60, 1, 55, "Input"],
Cell[18028, 683, 77, 1, 55, "Input"],
Cell[18108, 686, 261, 13, 60, "Text"],
Cell[18372, 701, 652, 18, 82, "DisplayFormulaNumbered"],
Cell[19027, 721, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[19108, 727, 147, 4, 68, "Subsection"],
Cell[19258, 733, 135, 4, 60, "Text"],
Cell[19396, 739, 98, 2, 55, "Input"],
Cell[19497, 743, 81, 2, 55, "Input"],
Cell[19581, 747, 600, 18, 83, "Input"],
Cell[20184, 767, 152, 4, 55, "Input"],
Cell[20339, 773, 97, 2, 55, "Input"],
Cell[20439, 777, 80, 2, 55, "Input"],
Cell[20522, 781, 82, 2, 55, "Input"],
Cell[20607, 785, 81, 2, 55, "Input"],
Cell[20691, 789, 80, 2, 55, "Input"],
Cell[20774, 793, 91, 2, 55, "Input"],
Cell[20868, 797, 267, 13, 60, "Text"],
Cell[21138, 812, 801, 22, 88, "DisplayFormulaNumbered"],
Cell[21942, 836, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[22023, 842, 156, 5, 68, "Subsection"],
Cell[22182, 849, 120, 4, 60, "Text"],
Cell[22305, 855, 80, 2, 55, "Input"],
Cell[22388, 859, 96, 2, 55, "Input"],
Cell[22487, 863, 632, 19, 83, "Input"],
Cell[23122, 884, 79, 2, 55, "Input"],
Cell[23204, 888, 95, 2, 55, "Input"],
Cell[23302, 892, 60, 1, 55, "Input"],
Cell[23365, 895, 76, 1, 55, "Input"],
Cell[23444, 898, 249, 13, 60, "Text"],
Cell[23696, 913, 917, 28, 126, "DisplayFormulaNumbered"],
Cell[24616, 943, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[24697, 949, 138, 4, 68, "Subsection"],
Cell[24838, 955, 126, 4, 60, "Text"],
Cell[24967, 961, 81, 2, 55, "Input"],
Cell[25051, 965, 101, 2, 55, "Input"],
Cell[25155, 969, 574, 16, 94, "Input"],
Cell[25732, 987, 80, 2, 55, "Input"],
Cell[25815, 991, 100, 2, 55, "Input"],
Cell[25918, 995, 61, 1, 55, "Input"],
Cell[25982, 998, 81, 1, 55, "Input"],
Cell[26066, 1001, 254, 13, 60, "Text"],
Cell[26323, 1016, 871, 24, 160, "DisplayFormulaNumbered"],
Cell[27197, 1042, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[27278, 1048, 116, 3, 68, "Subsection"],
Cell[27397, 1053, 147, 4, 60, "Text"],
Cell[27547, 1059, 82, 2, 55, "Input"],
Cell[27632, 1063, 111, 3, 55, "Input"],
Cell[27746, 1068, 82, 2, 55, "Input"],
Cell[27831, 1072, 81, 2, 55, "Input"],
Cell[27915, 1076, 80, 2, 55, "Input"],
Cell[27998, 1080, 616, 18, 55, "Input"],
Cell[28617, 1100, 163, 4, 55, "Input"],
Cell[28783, 1106, 81, 2, 55, "Input"],
Cell[28867, 1110, 110, 3, 55, "Input"],
Cell[28980, 1115, 92, 2, 55, "Input"],
Cell[29075, 1119, 121, 3, 55, "Input"],
Cell[29199, 1124, 266, 13, 60, "Text"],
Cell[29468, 1139, 1102, 30, 165, "DisplayFormulaNumbered"],
Cell[30573, 1171, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[30654, 1177, 105, 2, 68, "Subsection"],
Cell[30762, 1181, 137, 4, 60, "Text"],
Cell[30902, 1187, 82, 2, 55, "Input"],
Cell[30987, 1191, 114, 3, 55, "Input"],
Cell[31104, 1196, 1422, 39, 170, "Input"],
Cell[32529, 1237, 81, 2, 55, "Input"],
Cell[32613, 1241, 113, 3, 55, "Input"],
Cell[32729, 1246, 62, 1, 55, "Input"],
Cell[32794, 1249, 91, 1, 55, "Input"],
Cell[32888, 1252, 264, 13, 60, "Text"],
Cell[33155, 1267, 1169, 28, 280, "DisplayFormulaNumbered"],
Cell[34327, 1297, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[34408, 1303, 79, 0, 68, "Subsection"],
Cell[34490, 1305, 120, 4, 60, "Text"],
Cell[34613, 1311, 80, 2, 55, "Input"],
Cell[34696, 1315, 95, 2, 55, "Input"],
Cell[34794, 1319, 1404, 39, 170, "Input"],
Cell[36201, 1360, 79, 2, 55, "Input"],
Cell[36283, 1364, 94, 2, 55, "Input"],
Cell[36380, 1368, 60, 1, 55, "Input"],
Cell[36443, 1371, 75, 1, 55, "Input"],
Cell[36521, 1374, 206, 10, 60, "Text"],
Cell[36730, 1386, 1143, 28, 304, "DisplayFormulaNumbered"],
Cell[37876, 1416, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[37969, 1423, 102, 2, 92, "Section"],
Cell[CellGroupData[{
Cell[38096, 1429, 95, 2, 68, "Subsection"],
Cell[38194, 1433, 152, 8, 60, "Text"],
Cell[38349, 1443, 107, 3, 55, "Input"],
Cell[38459, 1448, 153, 5, 55, "Input"],
Cell[38615, 1455, 155, 5, 55, "Input"],
Cell[38773, 1462, 106, 3, 55, "Input"],
Cell[38882, 1467, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[38963, 1473, 90, 2, 68, "Subsection"],
Cell[CellGroupData[{
Cell[39078, 1479, 194, 7, 58, "Subsubsection"],
Cell[39275, 1488, 369, 14, 92, "Text"],
Cell[39647, 1504, 125, 3, 55, "Input"],
Cell[39775, 1509, 1939, 53, 284, "Input"],
Cell[41717, 1564, 157, 4, 55, "Input"],
Cell[41877, 1570, 97, 2, 55, "Input"],
Cell[41977, 1574, 81, 2, 55, "Input"],
Cell[42061, 1578, 48, 0, 59, "Text"],
Cell[42112, 1580, 104, 2, 55, "Input"],
Cell[42219, 1584, 140, 3, 55, "Input"],
Cell[42362, 1589, 153, 3, 55, "Input"],
Cell[42518, 1594, 170, 4, 55, "Input"],
Cell[42691, 1600, 154, 4, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[42882, 1609, 196, 7, 58, "Subsubsection"],
Cell[43081, 1618, 467, 20, 93, "Text"],
Cell[43551, 1640, 153, 4, 55, "Input"],
Cell[43707, 1646, 263, 8, 55, "Input"],
Cell[43973, 1656, 267, 8, 55, "Input"],
Cell[44243, 1666, 138, 4, 55, "Input"],
Cell[44384, 1672, 142, 4, 55, "Input"],
Cell[44529, 1678, 152, 4, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[44718, 1687, 58, 0, 58, "Subsubsection"],
Cell[44779, 1689, 120, 3, 55, "Input"],
Cell[44902, 1694, 113, 3, 55, "Input"],
Cell[45018, 1699, 118, 3, 55, "Input"],
Cell[45139, 1704, 111, 3, 55, "Input"],
Cell[45253, 1709, 120, 3, 55, "Input"],
Cell[45376, 1714, 76, 1, 55, "Input"],
Cell[45455, 1717, 104, 2, 55, "Input"],
Cell[45562, 1721, 127, 3, 55, "Input"],
Cell[45692, 1726, 180, 4, 55, "Input"],
Cell[45875, 1732, 256, 6, 55, "Input"],
Cell[46134, 1740, 219, 6, 55, "Input"],
Cell[46356, 1748, 420, 11, 94, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[46813, 1764, 96, 2, 58, "Subsubsection"],
Cell[46912, 1768, 895, 41, 126, "Text"],
Cell[47810, 1811, 125, 3, 55, "Input"],
Cell[47938, 1816, 1206, 36, 177, "Input"],
Cell[49147, 1854, 157, 4, 55, "Input"],
Cell[49307, 1860, 97, 2, 55, "Input"],
Cell[49407, 1864, 81, 2, 55, "Input"],
Cell[49491, 1868, 35, 0, 59, "Text"],
Cell[49529, 1870, 305, 7, 55, "Input"],
Cell[49837, 1879, 261, 7, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[50135, 1891, 97, 2, 58, "Subsubsection"],
Cell[50235, 1895, 570, 24, 93, "Text"],
Cell[50808, 1921, 125, 3, 55, "Input"],
Cell[50936, 1926, 1982, 50, 435, "Input"],
Cell[52921, 1978, 161, 4, 55, "Input"],
Cell[53085, 1984, 124, 3, 55, "Input"],
Cell[53212, 1989, 48, 0, 59, "Text"],
Cell[53263, 1991, 340, 8, 94, "Input"],
Cell[53606, 2001, 416, 12, 55, "Input"],
Cell[54025, 2015, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[54118, 2022, 55, 0, 68, "Subsection"],
Cell[54176, 2024, 99, 2, 59, "Text"],
Cell[54278, 2028, 120, 3, 55, "Input"],
Cell[54401, 2033, 113, 3, 55, "Input"],
Cell[54517, 2038, 119, 3, 55, "Input"],
Cell[54639, 2043, 112, 3, 55, "Input"],
Cell[54754, 2048, 144, 3, 55, "Input"],
Cell[54901, 2053, 128, 3, 55, "Input"],
Cell[55032, 2058, 140, 3, 55, "Input"],
Cell[55175, 2063, 124, 3, 55, "Input"],
Cell[55302, 2068, 180, 4, 55, "Input"],
Cell[55485, 2074, 157, 4, 55, "Input"],
Cell[55645, 2080, 256, 6, 55, "Input"],
Cell[55904, 2088, 219, 6, 55, "Input"],
Cell[56126, 2096, 305, 7, 55, "Input"],
Cell[56434, 2105, 261, 7, 55, "Input"],
Cell[56698, 2114, 340, 8, 94, "Input"],
Cell[57041, 2124, 287, 8, 55, "Input"],
Cell[57331, 2134, 341, 8, 94, "Input"],
Cell[57675, 2144, 417, 12, 55, "Input"],
Cell[58095, 2158, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[58188, 2165, 146, 5, 93, "Section"],
Cell[CellGroupData[{
Cell[58359, 2174, 144, 3, 68, "Subsection"],
Cell[58506, 2179, 361, 14, 93, "Text"],
Cell[58870, 2195, 85, 2, 55, "Input"],
Cell[58958, 2199, 744, 22, 98, "Input"],
Cell[59705, 2223, 84, 2, 55, "Input"],
Cell[59792, 2227, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[59873, 2233, 169, 5, 68, "Subsection"],
Cell[60045, 2240, 259, 6, 92, "Text"],
Cell[60307, 2248, 144, 4, 55, "Input"],
Cell[60454, 2254, 113, 3, 55, "Input"],
Cell[60570, 2259, 93, 2, 55, "Input"],
Cell[60666, 2263, 101, 2, 55, "Input"],
Cell[60770, 2267, 1250, 35, 246, "Input"],
Cell[62023, 2304, 100, 2, 55, "Input"],
Cell[62126, 2308, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[62219, 2315, 112, 3, 92, "Section"],
Cell[CellGroupData[{
Cell[62356, 2322, 93, 2, 68, "Subsection"],
Cell[CellGroupData[{
Cell[62474, 2328, 53, 0, 58, "Subsubsection"],
Cell[62530, 2330, 345, 7, 60, "Text"],
Cell[62878, 2339, 90, 2, 55, "Input"],
Cell[62971, 2343, 44, 0, 59, "Text"],
Cell[63018, 2345, 301, 8, 55, "Input"],
Cell[63322, 2355, 90, 2, 59, "Text"],
Cell[63415, 2359, 304, 10, 55, "Input"],
Cell[63722, 2371, 169, 7, 60, "Text"],
Cell[63894, 2380, 141, 3, 55, "Input"],
Cell[64038, 2385, 274, 9, 93, "Text"],
Cell[64315, 2396, 420, 12, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[64772, 2413, 52, 0, 58, "Subsubsection"],
Cell[64827, 2415, 389, 10, 60, "Text"],
Cell[65219, 2427, 90, 2, 55, "Input"],
Cell[65312, 2431, 301, 8, 55, "Input"],
Cell[65616, 2441, 81, 2, 55, "Input"],
Cell[65700, 2445, 331, 11, 55, "Input"],
Cell[66034, 2458, 141, 3, 55, "Input"],
Cell[66178, 2463, 59, 1, 55, "Input"],
Cell[66240, 2466, 420, 12, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[66697, 2483, 52, 0, 58, "Subsubsection"],
Cell[66752, 2485, 285, 8, 60, "Text"],
Cell[67040, 2495, 90, 2, 55, "Input"],
Cell[67133, 2499, 231, 6, 55, "Input"],
Cell[67367, 2507, 81, 2, 55, "Input"],
Cell[67451, 2511, 201, 7, 55, "Input"],
Cell[67655, 2520, 141, 3, 55, "Input"],
Cell[67799, 2525, 59, 1, 55, "Input"],
Cell[67861, 2528, 420, 12, 55, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[68318, 2545, 52, 0, 58, "Subsubsection"],
Cell[68373, 2547, 509, 16, 60, "Text"],
Cell[68885, 2565, 90, 2, 55, "Input"],
Cell[68978, 2569, 231, 6, 55, "Input"],
Cell[69212, 2577, 81, 2, 55, "Input"],
Cell[69296, 2581, 306, 10, 55, "Input"],
Cell[69605, 2593, 141, 3, 55, "Input"],
Cell[69749, 2598, 447, 13, 61, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[70233, 2616, 52, 0, 58, "Subsubsection"],
Cell[70288, 2618, 69, 1, 60, "Text"],
Cell[70360, 2621, 423, 9, 93, "Text"],
Cell[70786, 2632, 90, 2, 55, "Input"],
Cell[70879, 2636, 301, 8, 55, "Input"],
Cell[71183, 2646, 439, 14, 55, "Input"],
Cell[71625, 2662, 228, 8, 55, "Input"],
Cell[71856, 2672, 234, 7, 55, "Input"],
Cell[72093, 2681, 101, 2, 55, "Input"],
Cell[72197, 2685, 559, 16, 103, "Input"],
Cell[72759, 2703, 558, 16, 103, "Input"],
Cell[73320, 2721, 558, 16, 103, "Input"],
Cell[73881, 2739, 69, 1, 60, "Text"],
Cell[73953, 2742, 425, 9, 93, "Text"],
Cell[74381, 2753, 90, 2, 55, "Input"],
Cell[74474, 2757, 301, 8, 55, "Input"],
Cell[74778, 2767, 439, 14, 55, "Input"],
Cell[75220, 2783, 481, 16, 61, "Input"],
Cell[75704, 2801, 234, 7, 55, "Input"],
Cell[75941, 2810, 101, 2, 55, "Input"],
Cell[76045, 2814, 559, 16, 103, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[76641, 2835, 52, 0, 58, "Subsubsection"],
Cell[76696, 2837, 90, 2, 55, "Input"],
Cell[76789, 2841, 301, 8, 55, "Input"],
Cell[77093, 2851, 211, 7, 55, "Input"],
Cell[77307, 2860, 225, 7, 55, "Input"],
Cell[77535, 2869, 223, 7, 55, "Input"],
Cell[77761, 2878, 234, 7, 55, "Input"],
Cell[77998, 2887, 101, 2, 55, "Input"],
Cell[78102, 2891, 559, 16, 103, "Input"],
Cell[78664, 2909, 558, 16, 103, "Input"],
Cell[79225, 2927, 558, 16, 103, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[79820, 2948, 52, 0, 58, "Subsubsection"],
Cell[79875, 2950, 90, 2, 55, "Input"],
Cell[79968, 2954, 405, 10, 132, "Input"],
Cell[80376, 2966, 211, 7, 55, "Input"],
Cell[80590, 2975, 225, 7, 55, "Input"],
Cell[80818, 2984, 223, 7, 55, "Input"],
Cell[81044, 2993, 234, 7, 55, "Input"],
Cell[81281, 3002, 96, 2, 55, "Input"],
Cell[81380, 3006, 559, 16, 103, "Input"],
Cell[81942, 3024, 558, 16, 103, "Input"],
Cell[82503, 3042, 558, 16, 103, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[83098, 3063, 52, 0, 58, "Subsubsection"],
Cell[83153, 3065, 90, 2, 55, "Input"],
Cell[83246, 3069, 231, 6, 55, "Input"],
Cell[83480, 3077, 246, 8, 55, "Input"],
Cell[83729, 3087, 225, 7, 55, "Input"],
Cell[83957, 3096, 223, 7, 55, "Input"],
Cell[84183, 3105, 234, 7, 55, "Input"],
Cell[84420, 3114, 96, 2, 55, "Input"],
Cell[84519, 3118, 560, 16, 103, "Input"],
Cell[85082, 3136, 558, 16, 103, "Input"],
Cell[85643, 3154, 557, 16, 103, "Input"],
Cell[86203, 3172, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[86296, 3179, 106, 2, 68, "Subsection"],
Cell[CellGroupData[{
Cell[86427, 3185, 88, 1, 58, "Subsubsection"],
Cell[86518, 3188, 85, 2, 55, "Input"],
Cell[86606, 3192, 74, 2, 55, "Input"],
Cell[86683, 3196, 100, 3, 83, "Input"],
Cell[86786, 3201, 200, 5, 55, "Input"],
Cell[86989, 3208, 93, 2, 55, "Input"],
Cell[87085, 3212, 148, 4, 85, "Input"],
Cell[87236, 3218, 263, 8, 55, "Input"],
Cell[87502, 3228, 497, 14, 94, "Input"],
Cell[88002, 3244, 300, 9, 55, "Input"],
Cell[88305, 3255, 300, 9, 55, "Input"],
Cell[88608, 3266, 310, 9, 55, "Input"],
Cell[88921, 3277, 191, 5, 55, "Input"],
Cell[89115, 3284, 191, 5, 55, "Input"],
Cell[89309, 3291, 191, 5, 55, "Input"],
Cell[89503, 3298, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[89584, 3304, 102, 2, 58, "Subsubsection"],
Cell[89689, 3308, 85, 2, 55, "Input"],
Cell[89777, 3312, 74, 2, 55, "Input"],
Cell[89854, 3316, 100, 3, 83, "Input"],
Cell[89957, 3321, 200, 5, 55, "Input"],
Cell[90160, 3328, 93, 2, 55, "Input"],
Cell[90256, 3332, 148, 4, 85, "Input"],
Cell[90407, 3338, 517, 14, 94, "Input"],
Cell[90927, 3354, 310, 9, 55, "Input"],
Cell[91240, 3365, 310, 9, 55, "Input"],
Cell[91553, 3376, 310, 9, 55, "Input"],
Cell[91866, 3387, 191, 5, 55, "Input"],
Cell[92060, 3394, 191, 5, 55, "Input"],
Cell[92254, 3401, 191, 5, 55, "Input"],
Cell[92448, 3408, 60, 1, 55, "Input"],
Cell[92511, 3411, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[92592, 3417, 98, 2, 58, "Subsubsection"],
Cell[92693, 3421, 85, 2, 55, "Input"],
Cell[92781, 3425, 74, 2, 55, "Input"],
Cell[92858, 3429, 100, 3, 83, "Input"],
Cell[92961, 3434, 200, 5, 55, "Input"],
Cell[93164, 3441, 93, 2, 55, "Input"],
Cell[93260, 3445, 148, 4, 85, "Input"],
Cell[93411, 3451, 164, 4, 55, "Input"],
Cell[93578, 3457, 310, 9, 55, "Input"],
Cell[93891, 3468, 77, 2, 55, "Input"],
Cell[93971, 3472, 191, 5, 55, "Input"],
Cell[94165, 3479, 191, 5, 55, "Input"],
Cell[94359, 3486, 60, 1, 55, "Input"],
Cell[94422, 3489, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[94527, 3497, 121, 4, 93, "Section"],
Cell[CellGroupData[{
Cell[94673, 3505, 202, 7, 58, "Subsubsection"],
Cell[94878, 3514, 76, 2, 55, "Input"],
Cell[94957, 3518, 74, 2, 55, "Input"],
Cell[95034, 3522, 218, 7, 60, "Text"],
Cell[95255, 3531, 83, 2, 55, "Input"],
Cell[95341, 3535, 259, 8, 55, "Input"],
Cell[95603, 3545, 179, 7, 60, "Text"],
Cell[95785, 3554, 82, 2, 55, "Input"],
Cell[95870, 3558, 206, 7, 55, "Input"],
Cell[96079, 3567, 81, 2, 55, "Input"],
Cell[96163, 3571, 145, 6, 60, "Text"],
Cell[96311, 3579, 275, 9, 55, "Input"],
Cell[96589, 3590, 195, 6, 55, "Input"],
Cell[96787, 3598, 378, 17, 63, "Text"],
Cell[97168, 3617, 126, 3, 55, "Input"],
Cell[97297, 3622, 477, 15, 59, "Input"],
Cell[97777, 3639, 578, 18, 59, "Input"],
Cell[98358, 3659, 172, 5, 55, "Input"],
Cell[98533, 3666, 125, 3, 55, "Input"],
Cell[98661, 3671, 175, 8, 60, "Text"],
Cell[98839, 3681, 301, 10, 87, "Input"],
Cell[99143, 3693, 291, 9, 55, "Input"],
Cell[99437, 3704, 503, 14, 94, "Input"],
Cell[99943, 3720, 44, 1, 6, "PageBreak",
 PageBreakBelow->True]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[100036, 3727, 51, 0, 92, "Section"],
Cell[100090, 3729, 167, 5, 60, "Text"],
Cell[100260, 3736, 166, 5, 60, "Text"],
Cell[100429, 3743, 235, 6, 92, "Text"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature @wDfGcROQkyy9C1fcq7JsTgV *)
